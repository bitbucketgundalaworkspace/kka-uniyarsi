<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {

	function __construct()
	{
        // Construct the parent class
		parent::__construct();

        // Configure limits on our controller methods
		$this->load->database();
		$this->load->model('ion_auth_model');
	}

	public function index()
	{
		$this->session->before_login = "home";
		if($this->session->user_id)
		$this->session->group_user = $this->ion_auth_model->get_users_groups()->row()->id;
		$data['login'] = $this->ion_auth->is_admin();
		$this->load->view('form-ajuan', $data);
	}

	public function ajuan()
	{
		$this->session->before_login = "home";
		if($this->session->user_id)
		$this->session->group_user = $this->ion_auth_model->get_users_groups()->row()->id;
		$data['login'] = $this->ion_auth->is_admin();
		$this->load->view('form-ajuan', $data);
	}

	public function monitoring()
	{
		$this->session->before_login = "home";
		if($this->session->user_id)
		$this->session->group_user = $this->ion_auth_model->get_users_groups()->row()->id;
		$data['login'] = $this->ion_auth->is_admin();
		$this->load->view('form-monitoring', $data);
	}

	public function kepuasan()
	{
		$this->session->before_login = "home";
		if($this->session->user_id)
		$this->session->group_user = $this->ion_auth_model->get_users_groups()->row()->id;
		$data['login'] = $this->ion_auth->is_admin();
		$this->load->view('form-kepuasan', $data);
	}

	public function data_ajuan()
	{
		$this->session->before_login = "home";
		if($this->session->user_id)
		$this->session->group_user = $this->ion_auth_model->get_users_groups()->row()->id;
		$data['login'] = $this->ion_auth->is_admin();
		$data['ajuan'] = $this->db->get('ajuan')->result_array();
		$this->load->view('form-ajuan-admin', $data);
	}

	public function update_ajuan()
	{
		$nama_user = $this->db->get_where('users', array('id' => $this->session->userdata('user_id')))->result_array();
		$data = array('status' => $this->input->post('status'),
						'catatan' => $this->input->post('catatan'),
						'tgl_review' => date('Y-m-d'),
						'reviewer' => $nama_user[0]['first_name'],
						'id_kat' => $this->input->post('id_kategori'),
						'id_kerma' => $this->input->post('id_kerma'));
		
		if($this->db->update('ajuan', $data, array('id' => $this->input->post('id')))){
	        	$this->session->set_flashdata('success', TRUE);
	            redirect('Form/data_ajuan');
	        }else{
	        	$this->session->set_flashdata('error', TRUE);
	            redirect('Form/data_ajuan');
	        }
	}
}
