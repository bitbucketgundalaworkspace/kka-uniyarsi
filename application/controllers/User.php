<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
/*require APPPATH . 'libraries/REST_Controller.php';*/


class User extends CI_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        $this->load->library('datatables');
        $this->load->model('User_model');
    }

    public function index(){
        $this->session->before_login = "user";
        $data['login'] = $this->ion_auth->is_admin();
        $this->load->view('user', $data);
    }

    public function data(){
        header("Content-Type: application/json;charset=utf-8");
        @$id = $this->uri->segment('3');
        if($id == NULL){
            echo $this->User_model->user_get_all();
        }
        else{
            echo json_encode($this->User_model->user_get_by_id($id), JSON_PRETTY_PRINT);
        }
        
    }

    public function listData(){
        echo json_encode($this->User_model->get_list_data());
    }

    /*public function update(){
        $data = array( 
            'nama_internal' => $_POST['nama_internal'],
        );


        $id = $_POST['id'];
        echo $this->User_model->update($data, $id) ? "true" : "false";
        //echo "true"; //Buat Sementara di matikan
    }

    public function insert(){
        $data = array(
            'nama_internal' => $_POST['nama_internal'],
        );
        echo $this->User_model->insert($data) ? "true" : "false";
        //echo "true"; //Buat Sementara di matikan
    }

    public function delete(){
        $id = $_POST["id"];
        echo $this->User_model->delete($id) ? "true" : "false";
        //echo "true"; //Buat Sementara di matikan
    }
*/
    
}