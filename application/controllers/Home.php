<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
        // Construct the parent class
		parent::__construct();

        // Configure limits on our controller methods
		$this->load->database();
		$this->load->model('ion_auth_model');
	}

	public function index()
	{
		$this->session->before_login = "home";
		if($this->session->user_id)
		$this->session->group_user = $this->ion_auth_model->get_users_groups()->row()->id;
		$data['login'] = $this->ion_auth->is_admin();

		$dalam = $this->db->query('SELECT MONTH(start_date) MONTH, COUNT(*) COUNT
		FROM ks_dalam
		WHERE end_date != "'.date('Y-m-d').'"
		GROUP BY MONTH(start_date);')->result_array();
		$luar = $this->db->query('SELECT MONTH(start_date) MONTH, COUNT(*) COUNT
		FROM ks_luar
		WHERE end_date != "'.date('Y-m-d').'"
		GROUP BY MONTH(start_date);')->result_array();
		$m = array(); //bulan
		$cdalam = array(); //countdalam
		$cluar = array(); //countdalam
		foreach ($dalam as $key => $value) {
			if(bulan($value['MONTH']) != null){
				$m[] = bulan($value['MONTH']);
			$cdalam[] = $value['COUNT'];
			}
			
		}
		foreach ($luar as $key1 => $value1) {
			$luar[] = $value1['COUNT'];
		}
		$data['month'] = $m;
		$data['dalam'] = $cdalam;
		$data['luar'] = $luar;
		// print_r($this->session->all_userdata());
		$data['cekajuan'] = $this->db->where('status !=','Diterima')->from("ajuan")->count_all_results();
		$this->load->view('home', $data);
	}
}
