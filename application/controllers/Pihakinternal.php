<?php

defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
/*require APPPATH . 'libraries/REST_Controller.php';*/

class Pihakinternal extends CI_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        $this->load->library('datatables');
        $this->load->model('Pihakinternal_model');
    }

    public function index(){
        $this->session->before_login = "pihakinternal";
        $data['login'] = $this->ion_auth->is_admin();
        $this->load->view('pihakinternal', $data);
    }

    /*public function data(){
        //header("Content-Type: application/json;charset=utf-8");
        echo $this->Luarnegeri_model->luarnegeri_get_all();        
    }*/

    public function data(){
        header("Content-Type: application/json;charset=utf-8");
        @$id = $this->uri->segment('3');
        if($id == NULL){
            echo $this->Pihakinternal_model->pihakinternal_get_all();
        }
        else{
            echo json_encode($this->Pihakinternal_model->pihakinternal_get_by_id($id), JSON_PRETTY_PRINT);
        }
        
    }

    public function listData(){
        echo json_encode($this->Pihakinternal_model->get_list_data());
    }

    public function update(){
        if ($this->Pihakinternal_model->update(['nama_internal' => $_POST['nama_internal']], $_POST['id']) > 0) {
            echo "true";
        }else{
            echo "false";
        }
    }

    public function insert(){
        $data = array(
            'nama_internal' => $_POST['nama_internal'],
        );
        echo $this->Pihakinternal_model->insert($data) ? "true" : "false";
        //echo "true"; //Buat Sementara di matikan
    }

    public function delete(){
        $id = $_POST["id"];
        echo $this->Pihakinternal_model->delete($id) ? "true" : "false";
        //echo "true"; //Buat Sementara di matikan
    }

    
}