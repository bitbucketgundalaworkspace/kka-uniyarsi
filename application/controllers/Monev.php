<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Monev extends CI_Controller
{
    public function upload()
    {
		$countdb = $this->db->query('SELECT * FROM monev');
		$exp = explode(".", $_FILES["file_ajuan"]['name']);
		$nama_file 	="Monev-".$countdb->num_rows()."-".date("d-m-Y").".".$exp[1];
		$config['file_name'] 			= $nama_file;
		$config['upload_path']          = './uploads/monev';
	    $config['allowed_types']        = '*';
	    // $config['overwrite']        	= True;

	    $this->load->library('upload', $config);
        $this->upload->initialize($config);

	    if ( ! $this->upload->do_upload('file_ajuan'))
	    {
            $error = array('error' => $this->upload->display_errors());
	            print_r($error);
	    }
	    else
	    {
	        $data = array(
            'dokumen' 	=> $nama_file,
            'id_ks' 		=> $this->input->post('ks'),
            'tgl_monev' 	=> date("Y/m/d"),
			'jenis_monev'   => $this->input->post('jenis'),
			'jenis_ks'      => $this->input->post('jns'));
			// print_r($data);
			if($data['jenis_ks'] == 1){
				$tujuan = 'Dalamnegeri/monev/';
			}else{
				$tujuan = 'Luarnegeri/monev/';
			}
	        if($this->db->insert('monev', $data)){
	        	$this->session->set_flashdata('success', TRUE);
	            redirect($tujuan.$this->input->post('ks'));
	        }else{
	        	$this->session->set_flashdata('error', TRUE);
	            redirect($tujuan.$this->input->post('ks'));
	        }
	        	
	    }
    }

    public function monevcomen()
    {
        $nama_user = $this->db->get_where('users', array('id' => $this->session->userdata('user_id')))->result_array();
        $data = array(
            'reviewer' 	   => $nama_user[0]['first_name']." ".$nama_user[0]['last_name'],
            'catatan' 	   => $this->input->post('catatan'),
            'tgl_review' => date("Y/m/d"));
            // print_r($data);
           ($this->input->post('direct') == 'dalamnegeri') ? $direct = 'Dalamnegeri/monev/':$direct ='Luarnegeri/monev/'; 
	        if($this->db->update('monev', $data, array('id' => $this->input->post('id')))){
	        	$this->session->set_flashdata('success', TRUE);
	            redirect($direct.$this->input->post('ks'));
	        }else{
	        	$this->session->set_flashdata('error', TRUE);
	            redirect($direct.$this->input->post('ks'));
	        }
    }

    public function monevdelete()
    {
    	
        if($this->db->delete('monev', array('id' => $this->input->post('id')))){
            $this->session->set_flashdata('success', TRUE);
            redirect('Dalamnegeri/monev/'.$this->input->post('ks'));
        }else{
            $this->session->set_flashdata('error', TRUE);
            redirect('Dalamnegeri/monev/'.$this->input->post('ks'));
        }
        
    }
}
