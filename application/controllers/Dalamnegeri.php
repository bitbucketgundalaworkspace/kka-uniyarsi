<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
/*require APPPATH . 'libraries/REST_Controller.php';*/

class Dalamnegeri extends CI_Controller
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        $this->load->library('datatables');
        $this->load->model('Dalamnegeri_model');
        $this->load->model('Lihatkegiatandalam');
        $this->load->database();
    }
    public function index()
    {
        $this->session->before_login = "dalamnegeri";
        if ($this->input->get('idlm') == 1) { $data['tittle'] = 'Pemerintah'; }
        else if($this->input->get('idlm') == 2){ $data['tittle'] = 'Institusi Pendidikan'; }
        else if($this->input->get('idlm') == 3){ $data['tittle'] = 'Dunia Usaha'; }
        else{ $data['tittle'] = 'Organisasi'; }
        if ($this->session->user_id)
            $this->session->group_user = $this->ion_auth_model->get_users_groups()->row()->id;
        $data['login'] = $this->ion_auth->is_admin();
        $data['datadalam'] = $this->Dalamnegeri_model->dalamnegeri_get_all();
        // print_r($data['datadalam']);
        $this->load->view('dalamnegeri', $data);
    }
    public function export()
    {
        $this->session->before_login = "dalamnegeri";
        if ($this->session->user_id)
            $this->session->group_user = $this->ion_auth_model->get_users_groups()->row()->id;
        $data['login'] = $this->ion_auth->is_admin();
        $this->load->view('dalamnegeri-export', $data);
    }
    /*public function data(){
    
    //header("Content-Type: application/json;charset=utf-8");
    
    echo $this->Dalamnegeri_model->dalamnegeri_get_all();        
    
    }*/
    public function lihatkegiatan()
    {
        $this->session->before_login = "dalamnegeri";
        if ($this->uri->segment('3') == 1) { $data['kategori'] = 'Pemerintah'; }
        else if($this->uri->segment('3') == 2){ $data['kategori'] = 'Institusi Pendidikan'; }
        else if($this->uri->segment('3') == 3){ $data['kategori'] = 'Dunia Usaha'; }
        else{ $data['kategori'] = 'Organisasi'; }
        $data['login']               = $this->ion_auth->is_admin();
        $data['datakeg']             = $this->Lihatkegiatandalam->lihatkegiatandalam_get_all_by_id($this->uri->segment('4'));

        $this->load->view('lihatkegiatandalam', $data);
    }
    public function data_kegiatan()
    {
        header("Content-Type: application/json;charset=utf-8");
        @$id = $this->uri->segment('3');
        if ($id == NULL) {
            echo $this->Lihatkegiatandalam->lihatkegiatandalam_get_all();
        } else {
            echo $this->Lihatkegiatandalam->lihatkegiatandalam_get_all_by_id($id);
        }
    }
    public function data_kegiatan_lihat()
    {
        header("Content-Type: application/json;charset=utf-8");
        @$id = $this->uri->segment('3');
        echo json_encode($this->Lihatkegiatandalam->lihat_kegiatan_dalam_by_id($id));
    }
    public function data()
    {
        // header("Content-Type: application/json;charset=utf-8");
        $id = $this->uri->segment('3');
        if ($id == NULL) {
            echo $this->Dalamnegeri_model->dalamnegeri_get_all();
        } else {
            // echo json_encode($this->Dalamnegeri_model->dalamnegeri_get_by_id($id), JSON_PRETTY_PRINT);
            echo json_encode($this->Dalamnegeri_model->dalamnegeri_get_by_id($id));
        }
    }
    public function update()
    {
        if (!file_exists($_FILES['dok_asli']['tmp_name']) || !is_uploaded_file($_FILES['dok_asli']['tmp_name'])) {
            $data = array(
                'nama_internal' => $_POST['nama_internal'],
                'nama_eksternal' => $_POST['nama_eksternal'],
                'deskripsi' => $_POST['deskripsi'],
                'nomor' => $_POST['nomor'],
                //'lama' => $_POST['lama'],
                'start_date' => $_POST['start_date'],
                'end_date' => $_POST['end_date']
                /*'dok_asli' => $_POST['dok_asli'],*/
            );
        } else {
            $nama_dok_asli        = explode('.', $_FILES['dok_asli']['name']);
            $uploadOk             = 1;
            //Lokasi Upload
            $path_folder_dok_asli = "uploads/dalamnegeri/";
            //lokasi file upload + nama file + tanggal
            $countdb = $this->db->query('SELECT * FROM ks_dalam');  
            $newfilename = $path_folder_file."MoU_Dalamnegeri_".RndString(4).'_'.date('d-m-Y').'.'. $nama_dok_asli[1];
            //proses Upload
            move_uploaded_file($_FILES['dok_asli']['tmp_name'], $newfilename);
            $data = array(
                'nama_internal' => $_POST['nama_internal'],
                'nama_eksternal' => $_POST['nama_eksternal'],
                'deskripsi' => $_POST['deskripsi'],
                'nomor' => $_POST['nomor'],
                //'lama' => $_POST['lama'],
                'start_date' => $_POST['start_date'],
                'end_date' => $_POST['end_date'],
                'dok_asli' => $newfilename
            );
        }
        $id = $_POST['id_ksdalam'];
        echo $this->Dalamnegeri_model->update($data, $id) ? "true" : "false";
        //echo $this->db->last_query();
        //echo "true"; //Buat Sementara di matikan
    }
    public function update_kegiatan()
    {
        $data             = array(
            'tanggal' => $_POST['tanggal'],
            'uraian' => $_POST['uraian'],
            'persentase' => $_POST['persentase'],
            'file' => $_FILES['file_lampiran']['name']
        );
        //Deklarasi variable
        $nama_file        = explode('.', $_FILES['file_lampiran']['name']);
        $uploadOk         = 1;
        //Lokasi Upload
        $path_folder_file = "uploads/dalamnegeri_kegiatan/";
        //lokasi file upload + nama file + tanggal  
        $newfilename      = $path_folder_file."Kegiatan_Dalam_".RndString(4).'_'.date('d-m-Y').'.'.$nama_file[1];
        //proses Upload
        move_uploaded_file($_FILES['file_lampiran']['tmp_name'], $newfilename);
        $data = array(
            'tanggal' => $_POST['tanggal'],
            'uraian' => $_POST['uraian'],
            'persentase' => $_POST['persentase'],
            'file' => $newfilename
        );
        $id   = $this->input->post('id_kegiatandalam');
        //print_r($data);
        echo $this->Lihatkegiatandalam->update_kegiatan($data, $id) ? "true" : "false";
        //echo $this->db->last_query();
    }
    public function insert()
    {
        //Deklarasi variable
        //$uploadOk = 1;
        if ($_FILES['dok_asli']['name'] != null) {
            $countdb = $this->db->query('SELECT * FROM ks_dalam');
            $nama_file_dalam  = explode('.', $_FILES['dok_asli']['name']);
            //Lokasi Upload
            $path_folder_file = "uploads/dalamnegeri/";
            //lokasi file upload + nama file + tanggal  
            $newfilename = $path_folder_file."MoU_Dalamnegeri_".RndString(4).'_'.date('d-m-Y').'.'. $nama_file_dalam[1];
            //proses Upload
            move_uploaded_file($_FILES['dok_asli']['tmp_name'], $newfilename);
            $data = array(
                'nama_internal' => $_POST['nama_internal'],
                'nama_eksternal' => $_POST['nama_eksternal'],
                'deskripsi' => $_POST['deskripsi'],
                'nomor' => $_POST['nomor'],
                'id_kategori' => $_POST['id_kat'],
                //'lama' => $_POST['lama'],
                'start_date' => $_POST['start_date'],
                'end_date' => $_POST['end_date'],
                'dok_asli' => $newfilename
            );
        } else {
            $data = array(
                'nama_internal' => $_POST['nama_internal'],
                'nama_eksternal' => $_POST['nama_eksternal'],
                'deskripsi' => $_POST['deskripsi'],
                'nomor' => $_POST['nomor'],
                'id_kategori' => $_POST['id_kat'],
                'start_date' => $_POST['start_date'],
                'end_date' => $_POST['end_date']
            );
        }
        echo $this->Dalamnegeri_model->insert($data) ? "true" : "false";
        //header("location: https://maxelvutura.com/bidang4/dalamnegeri");
        //echo "true"; //Buat Sementara di matikan
    }
    public function delete_kegiatan()
    {
        $id_kegiatandalam = $_POST["id_kegiatandalam"];
        echo $this->Lihatkegiatandalam->delete_kegiatan($id_kegiatandalam) ? "true" : "false";
        //echo "true"; //Buat Sementara di matikan
    }
    public function delete()
    {
        $id_ksdalam = $_POST["id_ksdalam"];
        echo $this->Dalamnegeri_model->delete($id_ksdalam) ? "true" : "false";
        // echo "true"; //Buat Sementara di matikan
    }
    public function tambah_kegiatan()
    {
        $nama_file        = explode('.', $_FILES['file']['name']);
        $uploadOk         = 1;
        //Lokasi Upload
        $path_folder_file = "./uploads/dalamnegeri_kegiatan/";
        //lokasi file upload + nama file + tanggal  
        $newfilename      = $path_folder_file."Kegiatan_Dalam_".RndString(4).'_'.date('d-m-Y').'.'.$nama_file[1];
        move_uploaded_file($_FILES['file']['tmp_name'], $newfilename);
        $data = array(
            'tanggal' => $_POST['tanggal'],
            'uraian' => $_POST['uraian'],
            'persentase' => $_POST['persentase'],
            'file' => $newfilename,
            'id_ksdalam' => $_POST['id_ksdalam']
        );
        if ($this->Dalamnegeri_model->tambah_kegiatan($data)) {
            $this->session->set_flashdata('success', TRUE);
            redirect('Dalamnegeri?idlm='.$this->input->post('idlm'));
        } else {
            $this->session->set_flashdata('error', TRUE);
            redirect('Dalamnegeri?idlm='.$this->input->post('idlm'));
        }
    }

    public function monev()
    {
        $idks = $this->uri->segment('3');
        $data['monev'] = $this->db->get_where('monev', array('id_ks' => $idks,'jenis_ks' => 1))->result_array();
        $this->load->view('monitoring-evaluasi', $data);
    }

}