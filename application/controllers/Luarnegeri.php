<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
/*require APPPATH . 'libraries/REST_Controller.php';*/
/**        

*/

class Luarnegeri extends CI_Controller
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        $this->load->library('datatables');
        $this->load->model('Luarnegeri_model');
        $this->load->model('Lihatkegiatanluar');
        $this->load->database();
    }
    public function index()
    {
        $this->session->before_login = "luarnegeri";
        if ($this->input->get('idlm') == 1) { $data['tittle'] = 'Pemerintah'; }
        else if($this->input->get('idlm') == 2){ $data['tittle'] = 'Institusi Pendidikan'; }
        else if($this->input->get('idlm') == 3){ $data['tittle'] = 'Dunia Usaha'; }
        else{ $data['tittle'] = 'Organisasi'; }
        if ($this->session->user_id)
            $this->session->group_user = $this->ion_auth_model->get_users_groups()->row()->id;
        $data['login'] = $this->ion_auth->is_admin();
        $data['dataluar'] = $this->Luarnegeri_model->luarnegeri_get_all();
        $this->load->view('luarnegeri', $data);
    }
    public function export()
    {
        $this->session->before_login = "luarnegeri";
        if ($this->session->user_id)
            $this->session->group_user = $this->ion_auth_model->get_users_groups()->row()->id;
        $data['login'] = $this->ion_auth->is_admin();
        $this->load->view('luarnegeri-export', $data);
    }
    /*public function data(){
    
    //header("Content-Type: application/json;charset=utf-8");
    
    echo $this->Luarnegeri_model->luarnegeri_get_all();        
    
    }*/
    public function lihatkegiatan()
    {
        $this->session->before_login = "luarnegeri";
        if ($this->uri->segment('3') == 1) { $data['kategori'] = 'Pemerintah'; }
        else if($this->uri->segment('3') == 2){ $data['kategori'] = 'Institusi Pendidikan'; }
        else if($this->uri->segment('3') == 3){ $data['kategori'] = 'Dunia Usaha'; }
        else{ $data['kategori'] = 'Organisasi'; }
        $data['login']               = $this->ion_auth->is_admin();
        $data['datakeg']             = $this->Lihatkegiatanluar->lihatkegiatanluar_get_all_by_id($this->uri->segment('4'));
        $this->load->view('lihatkegiatanluar', $data);
    }
    public function data_kegiatan()
    {
        header("Content-Type: application/json;charset=utf-8");
        @$id = $this->uri->segment('3');
        if ($id == NULL) {
            echo $this->Lihatkegiatanluar->lihatkegiatanluar_get_all();
        } else {
            echo $this->Lihatkegiatanluar->lihatkegiatanluar_get_all_by_id($id);
        }
    }
    public function data_kegiatan_lihat()
    {
        header("Content-Type: application/json;charset=utf-8");
        @$id = $this->uri->segment('3');
        echo json_encode($this->Lihatkegiatanluar->lihat_kegiatan_luar_by_id($id));
    }
    public function data()
    {
        header("Content-Type: application/json;charset=utf-8");
        @$id = $this->uri->segment('3');
        if ($id == NULL) {
            echo $this->Luarnegeri_model->luarnegeri_get_all();
        } else {
            echo json_encode($this->Luarnegeri_model->luarnegeri_get_by_id($id), JSON_PRETTY_PRINT);
        }
    }
    public function update()
    {
        if (!file_exists($_FILES['dok_asli']['tmp_name']) || !is_uploaded_file($_FILES['dok_asli']['tmp_name'])) {
            $data = array(
                'nama_internal' => $_POST['nama_internal'],
                'nama_eksternal' => $_POST['nama_eksternal'],
                'deskripsi' => $_POST['deskripsi'],
                'nomor' => $_POST['nomor'],
                //'lama' => $_POST['lama'],
                'start_date' => $_POST['start_date'],
                'end_date' => $_POST['end_date']
                /*'dok_asli' => $_POST['dok_asli'],*/
            );
        } else {
            $nama_dok_asli        = explode('.', $_FILES['dok_asli']['name']);
            $uploadOk             = 1;
            //Lokasi Upload
            $path_folder_dok_asli = "./uploads/luarnegeri/";
            //lokasi file upload + nama file + tanggal  
            $countdb = $this->db->query('SELECT * FROM ks_luar');
            $newfilename  = $path_folder_file."MoU_Luarmnegeri_".RndString(4).'_'.date('d-m-Y').'.'. $nama_dok_asli[1];
            //proses Upload
            move_uploaded_file($_FILES['dok_asli']['tmp_name'], $newfilename);
            $data = array(
                'nama_internal' => $_POST['nama_internal'],
                'nama_eksternal' => $_POST['nama_eksternal'],
                'deskripsi' => $_POST['deskripsi'],
                'nomor' => $_POST['nomor'],
                //'lama' => $_POST['lama'],
                'start_date' => $_POST['start_date'],
                'end_date' => $_POST['end_date'],
                'dok_asli' => $newfilename
            );
        }
        $id = $_POST['id_ksluar'];
        echo $this->Luarnegeri_model->update($data, $id) ? "true" : "false";
        //echo $this->db->last_query();
        //echo "true"; //Buat Sementara di matikan
    }
    public function update_kegiatan()
    {
        if (!file_exists($_FILES['file']['tmp_name']) || !is_uploaded_file($_FILES['file']['tmp_name'])) {
            $data = array(
                'tanggal' => $_POST['tanggal'],
                'uraian' => $_POST['uraian'],
                'persentase' => $_POST['persentase']
            );
        } else {
            $nama_file        = explode('.', $_FILES['file']['name']);
            $uploadOk         = 1;
            //Lokasi Upload
            $path_folder_file = "./uploads/luarnegeri_kegiatan/";
            //lokasi file upload + nama file + tanggal  
            $newfilename      = $path_folder_file."Kegiatan_Luar_".RndString(4).'_'.date('d-m-Y').'.'.$nama_file[1];
            //proses Upload
            move_uploaded_file($_FILES['file']['tmp_name'], $newfilename);
            $data = array(
                'tanggal' => $_POST['tanggal'],
                'uraian' => $_POST['uraian'],
                'persentase' => $_POST['persentase'],
                'file' => $newfilename
            );
        }
        $id = $_POST['id_kegiatanluar'];
        echo $this->Lihatkegiatanluar->update_kegiatan($data, $id) ? "true" : "false";
    }
    public function insert()
    {
        //Deklarasi variable
        //$uploadOk = 1;
        //Lokasi Upload
        if ($_FILES['dok_asli']['name'] != null) {
            $nama_file_luar   = explode('.', $_FILES['dok_asli']['name']);
            $path_folder_file = "uploads/luarnegeri/";
            //lokasi file upload + nama file + tanggal  
            $countdb = $this->db->query('SELECT * FROM ks_luar');
            $newfilename  = $path_folder_file."MoU_Luarmnegeri_".RndString(4).'_'.date('d-m-Y').'.'. $nama_file_luar[1];
            //proses Upload
            move_uploaded_file($_FILES['dok_asli']['tmp_name'], $newfilename);
            $data = array(
                'id_kategori' => $_POST['id_kat'],
                'nama_internal' => $_POST['nama_internal'],
                'nama_eksternal' => $_POST['nama_eksternal'],
                'deskripsi' => $_POST['deskripsi'],
                'nomor' => $_POST['nomor'],
                'start_date' => $_POST['start_date'],
                'end_date' => $_POST['end_date'],
                'dok_asli' => $newfilename
            );
        } else {
            $data = array(
                'id_kategori' => $_POST['id_kat'],
                'nama_internal' => $_POST['nama_internal'],
                'nama_eksternal' => $_POST['nama_eksternal'],
                'deskripsi' => $_POST['deskripsi'],
                'nomor' => $_POST['nomor'],
                'start_date' => $_POST['start_date'],
                'end_date' => $_POST['end_date']
            );
        }
        echo ($this->Luarnegeri_model->insert($data)) ? "true" : "false";
        //header("location: https://maxelvutura.com/bidang4/luarnegeri");
        //echo "true"; //Buat Sementara di matikan
    }
    public function delete_kegiatan()
    {
        $id_kegiatanluar = $_POST["id_kegiatanluar"];
        echo $this->Lihatkegiatanluar->delete_kegiatan($id_kegiatanluar) ? "true" : "false";
        //echo "true"; //Buat Sementara di matikan
    }
    public function delete()
    {
        $id_ksluar = $_POST["id_ksluar"];
        echo $this->Luarnegeri_model->delete($id_ksluar) ? "true" : "false";
        // echo "true"; //Buat Sementara di matikan
    }
    public function monev()
    {
        $idks = $this->uri->segment('3');
        $data['monev'] = $this->db->get_where('monev', array('id_ks' => $idks,'jenis_ks' => 2))->result_array();
        $this->load->view('monitoring-evaluasi', $data);
    }
    public function tambah_kegiatan()
    {
        //Deklarasi variable
        $nama_file        = explode('.', $_FILES['file']['name']);
        $uploadOk         = 1;
        //Lokasi Upload
        $path_folder_file = "./uploads/luarnegeri_kegiatan/";
        //lokasi file upload + nama file + tanggal  
        $newfilename      = $path_folder_file."Kegiatan_Luar_".RndString(4).'_'.date('d-m-Y').'.'.$nama_file[1];

        //proses Upload
        move_uploaded_file($_FILES['file']['tmp_name'], $newfilename);
        $data = array(
            'tanggal' => $_POST['tanggal'],
            'uraian' => $_POST['uraian'],
            'persentase' => $_POST['persentase'],
            'file' => $newfilename,
            'id_ksluar' => $_POST['id_ksluar']
        );

        if ($this->Luarnegeri_model->tambah_kegiatan($data)) {
            $this->session->set_flashdata('success', TRUE);
            redirect('Luarnegeri?idlm='.$this->input->post('idlm'));
        } else {
            $this->session->set_flashdata('error', TRUE);
            redirect('Luarnegeri?idlm='.$this->input->post('idlm'));
        }
        //}
    }
}