<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formulir extends CI_Controller {

	function __construct()
	{
        // Construct the parent class
		parent::__construct();

        // Configure limits on our controller methods
		$this->load->database();
		$this->load->model('ion_auth_model');
		$this->load->model('Dalamnegeri_model');
        $this->load->model('Lihatkegiatandalam');
	}

	public function index()
	{
		$this->session->before_login = "home";
		if($this->session->user_id)
		$this->session->group_user = $this->ion_auth_model->get_users_groups()->row()->id;
		$data['login'] = $this->ion_auth->is_admin();
		$data['ajuan'] = $this->db->get_where('ajuan', array('id_user' => $this->session->userdata('user_id')))->result_array();
		// $this->load->view('home', $data);
		$this->load->view('user-profil', $data);
	}

	public function upload_ajuan()
	{
		$nama_user = $this->db->get_where('users', array('id' => $this->session->userdata('user_id')))->result_array();
		$countdb = $this->db->query('SELECT * FROM ajuan WHERE id_user = "'.$this->session->userdata('user_id').'"');
		$exp = explode(".", $_FILES["file_ajuan"]['name']);
		$nama_file 	="Ajuan-".$countdb->num_rows()."-".$nama_user[0]['id']."-".date("d-m-Y").".".$exp[1];
		$config['file_name'] 			= $nama_file;
		$config['upload_path']          = 'uploads/ajuan';
	    $config['allowed_types']        = '*';
	    // $config['overwrite']        	= True;

	    $this->load->library('upload', $config);
        $this->upload->initialize($config);

	    if ( ! $this->upload->do_upload('file_ajuan'))
	    {
	            $error = array('error' => $this->upload->display_errors());
	            print_r($error);
	            // $this->load->view('upload_form', $error);
	            // $this->session->set_flashdata('error', TRUE);
	            // redirect('Formulir');
	    }
	    else
	    {
	        $data = array('dokumen' 	=> $nama_file,
	        			  'id_user' 		=> $this->session->userdata('user_id'),
						  'tgl_ajuan' 	=> date("Y/m/d"),
						  'jenis_ajuan' => $this->input->post('jenis'),
						  'status' 		=> 'Diproses');
	        if($this->db->insert('ajuan', $data)){
	        	$this->session->set_flashdata('success', TRUE);
	            redirect('Formulir');
	        }else{
	        	$this->session->set_flashdata('error', TRUE);
	            redirect('Formulir');
	        }
	        	
	    }
	}
	public function update_ajuan()
	{
		$nama_user = $this->db->get_where('users', array('id' => $this->session->userdata('user_id')))->result_array();
		$countdb = $this->db->query('SELECT * FROM ajuan WHERE id_user = "'.$this->session->userdata('user_id').'"');
		$exp = explode(".",$_FILES["file_ajuan"]['name']);
		$nama_file 	="Ajuan-".$countdb->num_rows()."-".$nama_user[0]['first_name']."-".date("d-m-Y").".".$exp[1];
		$config['file_name'] 			= $nama_file;
		$config['upload_path']          = './uploads/ajuan';
	    $config['allowed_types']        = '*';
	    // $config['overwrite']        	= True;

	    $this->load->library('upload', $config);
        $this->upload->initialize($config);

	    if ( ! $this->upload->do_upload('file_ajuan'))
	    {
	            $error = array('error' => $this->upload->display_errors());
	            print_r($error);
	            // $this->load->view('upload_form', $error);
	            // $this->session->set_flashdata('error', TRUE);
	            // redirect('Formulir');
	    }
	    else
	    {
	        $data = array('dokumen' 	=> $nama_file,
						  'status' 		=> 'Selesai Diperbaiki');
	        if($this->db->update('ajuan', $data, ['id' => $this->input->post('id')])){
	        	$this->session->set_flashdata('success', TRUE);
	            redirect('Formulir');
	        }else{
	        	$this->session->set_flashdata('error', TRUE);
	            redirect('Formulir');
	        }
	        	
	    }
	}

    public function update_kepuasan()
    {
        $nama_user = $this->db->get_where('users', array('id' => $this->session->userdata('user_id')))->result_array();
        $countdb = $this->db->query('SELECT * FROM ajuan WHERE id_user = "'.$this->session->userdata('user_id').'"');
        $exp = explode(".",$_FILES["file_ajuan"]['name']);
        $nama_file  ="Kepuasan-".$countdb->num_rows()."-".$nama_user[0]['id']."-".date("d-m-Y").".".$exp[1];
        $config['file_name']            = $nama_file;
        $config['upload_path']          = './uploads/kepuasan';
        $config['allowed_types']        = '*';
        // $config['overwrite']         = True;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('file_ajuan'))
        {
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
                // $this->load->view('upload_form', $error);
                // $this->session->set_flashdata('error', TRUE);
                // redirect('Formulir');
        }
        else
        {
            $data = array('dok_kepuasan' => $nama_file,
                          'tgl_kepuasan' => date("Y/m/d"));
            if($this->db->update('ajuan', $data, ['id' => $this->input->post('id')])){
                $this->session->set_flashdata('success', TRUE);
                redirect('Formulir');
            }else{
                $this->session->set_flashdata('error', TRUE);
                redirect('Formulir');
            }
                
        }
    }

	public function insertdalam()
    {
        //Deklarasi variable
        //$uploadOk = 1;
        if ($_FILES['dok_asli']['name'] != null) {
            $nama_file_dalam  = explode('.', $_FILES['dok_asli']['name']);
            //Lokasi Upload
            $path_folder_file = "uploads/dalamnegeri/";
            //lokasi file upload + nama file + tanggal  
            $newfilename      = $path_folder_file . "Ajuan_Dalam_".RndString(4) . '_' . date('d-m-Y') . '.' . $nama_file_dalam[1];
            //proses Upload
            move_uploaded_file($_FILES['dok_asli']['tmp_name'], $newfilename);
            $data = array(
                'nama_internal' => $_POST['nama_internal'],
                'id_ajuan' => $_POST['id_aju'],
                'nama_eksternal' => $_POST['nama_eksternal'],
                'deskripsi' => $_POST['deskripsi'],
                'nomor' => $_POST['nomor'],
                'id_kategori' => $_POST['id_kat'],
                //'lama' => $_POST['lama'],
                'start_date' => $_POST['start_date'],
                'end_date' => $_POST['end_date'],
                'dok_asli' => $newfilename
            );
        } else {
            $data = array(
                'id_ajuan' => $_POST['id_aju'],
                'nama_internal' => $_POST['nama_internal'],
                'nama_eksternal' => $_POST['nama_eksternal'],
                'deskripsi' => $_POST['deskripsi'],
                'nomor' => $_POST['nomor'],
                'id_kategori' => $_POST['id_kat'],
                'start_date' => $_POST['start_date'],
                'end_date' => $_POST['end_date']
            );
        }
        
        if ($this->db->insert('ks_dalam', $data)) {
            $this->session->set_flashdata('success', TRUE);
            redirect('Form/data_ajuan');
        } else {
            $this->session->set_flashdata('error', TRUE);
            redirect('Form/data_ajuan');
        }
        
    }

    public function insertluar()
    {
        //Deklarasi variable
        //$uploadOk = 1;
        //Lokasi Upload
        if ($_FILES['dok_asli']['name'] != null) {
            $nama_file_luar   = explode('.', $_FILES['dok_asli']['name']);
            $path_folder_file = "./uploads/luarnegeri/";
            //lokasi file upload + nama file + tanggal  
            $newfilename      = $path_folder_file . "Ajuan_Dalam_".RndString(4) . '_' . date('d-m-Y') . '.' . $nama_file_luar[1];
            //proses Upload
            move_uploaded_file($_FILES['dok_asli']['tmp_name'], $newfilename);
            $data = array(
                'id_ajuan' => $_POST['id_aju'],
                'nama_internal' => $_POST['nama_internal'],
                'nama_eksternal' => $_POST['nama_eksternal'],
                'deskripsi' => $_POST['deskripsi'],
                'id_kategori' => $_POST['id_kat'],
                'nomor' => $_POST['nomor'],
                'start_date' => $_POST['start_date'],
                'end_date' => $_POST['end_date'],
                'dok_asli' => $newfilename
            );
        } else {
            $data = array(
                'id_ajuan' => $_POST['id_aju'],
                'nama_internal' => $_POST['nama_internal'],
                'nama_eksternal' => $_POST['nama_eksternal'],
                'deskripsi' => $_POST['deskripsi'],
                'nomor' => $_POST['nomor'],
                'id_kategori' => $_POST['id_kat'],
                'start_date' => $_POST['start_date'],
                'end_date' => $_POST['end_date']
            );
        }
        if ($this->db->insert('ks_luar', $data)) {
            $this->session->set_flashdata('success', TRUE);
            redirect('Form/data_ajuan');
        } else {
            $this->session->set_flashdata('error', TRUE);
            redirect('Form/data_ajuan');
        }
    }
}
