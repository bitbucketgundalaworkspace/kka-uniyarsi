<?php
class Pihakinternal_model extends CI_Model { 

    public function __construct()
    {
        parent::__construct();
                // Your own constructor code
    }

    public function pihakinternal_get_all(){

        $this->datatables->select('id, nama_internal');
        $this->datatables->from('tb_pihakinternal');
        $this->datatables->add_column('Aksi', '<button class="btn btn-primary btn-sm showEdit" data-id="$1" title="Ubah" ><i class="fa fa-edit"></i></button>&nbsp;<button class="btn btn-danger btn-sm showDelete" data-id="$1" title="Hapus"><i class="fa fa-trash"></i></button>', 'id');
        return $this->datatables->generate();
    }

    public function pihakinternal_get_by_id($id){
        return $this->db->get_where('tb_pihakinternal', array('id' => $id))->result();
    }

    public function get_list_data(){
        return $this->db->get('tb_pihakinternal')->result();
    }

    public function insert($data){
        $this->db->trans_start();

        $this->db->insert('tb_pihakinternal', $data);

        $this->db->trans_complete();

        if($this->db->affected_rows() > 0){
            return true;
        }
        else{
            if($this->db->trans_status() === false){
                return false;
            }
            else{
                return true;
            }
        }
    }

    public function update($data, $id){
         $this->db->where('id', $id);
         $this->db->update('tb_pihakinternal', $data);
         return $this->db->affected_rows();
        // $this->db->trans_start();
        // if($this->check_duplicate($data["id"]) != 1){
        //     $this->db->where('id', $id);
        //     $this->db->update('tb_pihakinternal', $data);
        // }
        // else{
        //     return false;
        // }
        // $this->db->trans_complete();

        // if($this->db->affected_rows() > 0){
        //     return true;
        // }
        // else{
        //     if($this->db->trans_status() === false){
        //         return false;
        //     }
        //     else{
        //         return true;
        //     }
        // }
    }

    public function check_duplicate($id){
        return $this->db->where('id', $id)->from("tb_pihakinternal")->count_all_results();
    }

    public function delete($id){
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete('tb_pihakinternal');
        $this->db->trans_complete();

        if($this->db->affected_rows() > 0){
            return true;
        }
        else{
            if($this->db->trans_status() === false){
                return false;
            }
            else{
                return true;
            }
        }
    }

}