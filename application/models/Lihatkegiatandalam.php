<?php
class Lihatkegiatandalam extends CI_Model {

    public function __construct()
    {
        parent::__construct();
                // Your own constructor code
    }

    public function lihatkegiatandalam_get_all(){

        $this->datatables->select('id_kegiatandalam, id_ksdalam, tanggal, uraian, persentase, file');
        $this->datatables->from('kegiatan_dalam');
        $this->datatables->add_column('Aksi', '<button class="btn btn-primary btn-sm showEdit" data-id="$1" title="Ubah" ><i class="glyphicon glyphicon-pencil"></i></button>&nbsp;<button class="btn btn-danger btn-sm showDelete" data-id="$1" title="Hapus"><i class="glyphicon glyphicon-trash"></i></button>', 'id_ksdalam');
        return $this->datatables->generate();
    }
    public function lihatkegiatandalam_get_all_by_id($id){

        // $this->datatables->select('id_kegiatandalam, id_ksdalam, tanggal, uraian, persentase, file');
        // $this->datatables->from('kegiatan_dalam');
        // $this->datatables->where('id_ksdalam', $id);
        // $this->datatables->add_column('Aksi', '<button class="btn btn-primary btn-sm showEdit" data-id="$1" title="Ubah" ><i class="glyphicon glyphicon-pencil"></i></button>&nbsp;<button class="btn btn-danger btn-sm showDelete" data-id="$1" title="Hapus"><i class="glyphicon glyphicon-trash"></i></button>', 'id_kegiatandalam');
        // return $this->datatables->generate();

        $this->db->select('id_kegiatandalam, id_ksdalam, tanggal, uraian, persentase, file');
        $this->db->from('kegiatan_dalam');
        $this->db->where('id_ksdalam', $id);
        $this->db->order_by('id_ksdalam', 'DESC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function lihat_kegiatan_dalam_by_id($id){
        return $this->db->get_where('kegiatan_dalam', array('id_kegiatandalam' => $id))->result();
    }

    public function lihatkegiatandalam_get_by_id($id_kegiatandalam){
        return $this->db->get_where('kegiatan_dalam', array('id_kegiatandalam' => $id_kegiatandalam))->result();
    }

    public function update_kegiatan($data_kegiatan_lihat, $id){
        $this->db->where('id_kegiatandalam', $id);
        return $this->db->update('kegiatan_dalam', $data_kegiatan_lihat);
    }
    public function check_duplicate($id_ksdalam){
        return $this->db->where('id_ksdalam', $id_ksdalam)->from("ks_dalam")->count_all_results();
    }

    public function insert($data){
        $this->db->trans_start();
            //if($this->check_duplicate($data["id_ksdalam"]) != 1){
        $this->db->insert('ks_dalam', $data);
            /*}
            else{
                return false;
            }*/
            $this->db->trans_complete();

            if($this->db->affected_rows() > 0){
                return true;
            }
            else{
                if($this->db->trans_status() === false){
                    return false;
                }
                else{
                    return true;
                }
            }
        }
        public function delete_kegiatan($id_kegiatandalam){
            $this->db->trans_start();
            $this->db->where('id_kegiatandalam', $id_kegiatandalam);
            $this->db->delete('kegiatan_dalam');
            $this->db->trans_complete();

            if($this->db->affected_rows() > 0){
                return true;
            }
            else{
                if($this->db->trans_status() === false){
                    return false;
                }
                else{
                    return true;
                }
            }
        }

        public function tambah_kegiatan($data){
            return $this->db->insert('kegiatan_dalam', $data);
        }
    }