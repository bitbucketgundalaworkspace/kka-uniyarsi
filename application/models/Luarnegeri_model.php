<?php
class Luarnegeri_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
                // Your own constructor code
    }
    
    public function luarnegeri_get_all(){
        
        // $this->datatables->select('a.id_ksluar, a.nama_internal, a.nama_eksternal, a.deskripsi, a.nomor, a.lama, a.start_date, a.end_date, a.dok_asli,  b.tanggal, b.uraian, b.persentase, b.file, (SELECT COUNT(b.id_ksluar) FROM `kegiatan_luar` b WHERE a.id_ksluar = b.id_ksluar) AS `total_kegiatan`');
        // $this->datatables->from('ks_luar a, kegiatan_luar b');
        // $this->datatables->group_by('a.id_ksluar');
        // $this->datatables->add_column('Aksi', '<button class="btn btn-primary btn-sm showEdit" data-id="$1" title="Ubah" ><i class="glyphicon glyphicon-pencil"></i></button>&nbsp;<button class="btn btn-info btn-sm showAddKegiatan" data-id="$1" data-start="$2" data-end="$3" title="Tambah Kegiatan" ><i class="glyphicon glyphicon-plus"></i></button>&nbsp;<button class="btn btn-danger btn-sm showDelete" data-id="$1" title="Hapus"><i class="glyphicon glyphicon-trash"></i></button>', 'id_ksluar, start_date, end_date');
        // return $this->datatables->generate();

        

         $this->db->select('a.id_ksluar,a.id_ajuan, a.nama_internal, a.nama_eksternal, a.deskripsi, a.nomor, a.lama, a.start_date, a.end_date, a.dok_asli,  b.tanggal, b.uraian, b.persentase, b.file, (SELECT COUNT(b.id_ksluar) FROM `kegiatan_luar` b WHERE a.id_ksluar = b.id_ksluar) AS `total_kegiatan`');
        $this->db->from('ks_luar a, kegiatan_luar b');
        $this->db->group_by('a.id_ksluar');
        $this->db->order_by('id_ksluar', 'DESC');
        // $this->db->join('ajuan', 'ajuan.id = a.id_ajuan');
        $this->db->where('id_kategori', $this->input->get('idlm'));
        
        $query = $this->db->get()->result_array();
        return $query;
        // return $this->datatables->generate();
    }
    
    public function luarnegeri_get_by_id($id_ksluar){
        return $this->db->get_where('ks_luar', array('id_ksluar' => $id_ksluar))->result();
    }

    public function update($data, $id_ksluar){
      /*  $this->db->trans_start();
        if($this->check_duplicate($data["id_ksluar"]) != 1){
            $this->db->where('id_ksluar', $id_ksluar );
            $this->db->update('ks_luar', $data);
        }
        else{
            return false;
        }
        $this->db->trans_complete();

        if($this->db->affected_rows() > 0){
            return true;
        }
        else{
            if($this->db->trans_status() === false){
                return false;
            }
            else{
                return true;
            }
        }*/
        $this->db->where('id_ksluar', $id_ksluar);
        return $this->db->update('ks_luar', $data);
    }

    public function check_duplicate($id_ksluar){
        return $this->db->where('id_ksluar', $id_ksluar)->from("ks_luar")->count_all_results();
    }

    public function insert($data){
        $this->db->trans_start();
            //if($this->check_duplicate($data["id_ksluar"]) != 1){
        $this->db->insert('ks_luar', $data);
            /*}
            else{
                return false;
            }*/
            $this->db->trans_complete();

            if($this->db->affected_rows() > 0){
                return true;
            }
            else{
                if($this->db->trans_status() === false){
                    return false;
                }
                else{
                    return true;
                }
            }
        }

        public function delete($id_ksluar){
            $this->db->trans_start();
            $this->db->where('id_ksluar', $id_ksluar);
             $this->db->delete('ks_luar');
            $this->db->trans_complete();

            if($this->db->affected_rows() > 0){
                return true;
            }
            else{
                if($this->db->trans_status() === false){
                    return false;
                }
                else{
                    return true;
                }
            }
        }

        public function tambah_kegiatan($data){
            return $this->db->insert('kegiatan_luar', $data);
        }
    }