<?php
class Dalamnegeri_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
                // Your own constructor code
    }

    public function dalamnegeri_get_all(){

        // $this->datatables->select('a.id_ksdalam, a.nama_internal, a.nama_eksternal, a.deskripsi, a.nomor, a.lama, a.start_date, a.end_date, a.dok_asli,  b.tanggal, b.uraian, b.persentase, b.file, (SELECT COUNT(b.id_ksdalam) FROM `kegiatan_dalam` b WHERE a.id_ksdalam = b.id_ksdalam) AS `total_kegiatan`');
        // $this->datatables->from('ks_dalam a, kegiatan_dalam b');
        // $this->datatables->group_by('a.id_ksdalam');
        // $this->datatables->add_column('Dokumen', '<button class="btn btn-primary btn-sm showEdit" data-id="$1" title="Upload Dokumen" >Lihat Dokumen</i></button>', 'id_ksdalam, start_date, end_date');
        // $this->datatables->add_column('Aksi', '<button class="btn btn-primary btn-sm showEdit" data-id="$1" title="Ubah" ><i class="glyphicon glyphicon-pencil"></i></button>&nbsp;<button class="btn btn-info btn-sm showAddKegiatan" data-id="$1" data-start="$2" data-end="$3" title="Tambah Kegiatan" ><i class="glyphicon glyphicon-plus"></i></button>&nbsp;<button class="btn btn-danger btn-sm showDelete" data-id="$1" title="Hapus"><i class="glyphicon glyphicon-trash"></i></button>', 'id_ksdalam, start_date, end_date');

        $this->db->select('a.id_ksdalam,a.id_ajuan, a.nama_internal, a.nama_eksternal, a.deskripsi, a.nomor, a.lama, a.start_date, a.end_date, a.dok_asli,  b.tanggal, b.uraian, b.persentase, b.file, (SELECT COUNT(b.id_ksdalam) FROM `kegiatan_dalam` b WHERE a.id_ksdalam = b.id_ksdalam) AS `total_kegiatan`');
        $this->db->from('ks_dalam a, kegiatan_dalam b');
        $this->db->group_by('a.id_ksdalam');
        $this->db->order_by('id_ksdalam', 'DESC');
        // $this->db->join('ajuan', 'ajuan.id = a.id_ajuan');
        $this->db->where('id_kategori', $this->input->get('idlm'));
        
        $query = $this->db->get()->result_array();
        return $query;
        // return $this->datatables->generate();
    }

    public function dalamnegeri_get_by_id($id_ksdalam){
        return $this->db->get_where('ks_dalam', array('id_ksdalam' => $id_ksdalam))->result();
    }

    public function update($data, $id_ksdalam){
        /*$this->db->trans_start();
        if($this->check_duplicate($data["id_ksdalam"]) != 1){
            $this->db->where('id_ksdalam', $id_ksdalam);
            $this->db->update('ks_dalam', $data);
        }
        else{
            return false;
        }
        $this->db->trans_complete();

        if($this->db->affected_rows() > 0){
            return true;
        }
        else{
            if($this->db->trans_status() === false){
                return false;
            }
            else{
                return true;
            }
        }*/
        $this->db->where('id_ksdalam', $id_ksdalam);
        return $this->db->update('ks_dalam', $data);
    }

    public function check_duplicate($id_ksdalam){
        return $this->db->where('id_ksdalam', $id_ksdalam)->from("ks_dalam")->count_all_results();
    }

    public function insert($data){
        $this->db->trans_start();
            //if($this->check_duplicate($data["id_ksdalam"]) != 1){
        $this->db->insert('ks_dalam', $data);
            /*}
            else{
                return false;
            }*/
            $this->db->trans_complete();

            if($this->db->affected_rows() > 0){
                return true;
            }
            else{
                if($this->db->trans_status() === false){
                    return false;
                }
                else{
                    return true;
                }
            }
        }

         public function delete($id_ksdalam){
            $this->db->trans_start();
            $this->db->where('id_ksdalam', $id_ksdalam);
            $this->db->delete('ks_dalam');
            $this->db->trans_complete();

            if($this->db->affected_rows() > 0){
                return true;
            }
            else{
                if($this->db->trans_status() === false){
                    return false;
                }
                else{
                    return true;
                }
            }
        }
        public function tambah_kegiatan($data){
            return $this->db->insert('kegiatan_dalam', $data);
        }
    }