<?php
class User_model extends CI_Model { 

    public function __construct()
    {
        parent::__construct();
                // Your own constructor code
    }

    public function user_get_all(){

        $this->datatables->select('id, first_name, email, phone, company');
        $this->datatables->from('users');
        $this->datatables->add_column('Aksi', '<button class="btn btn-primary btn-sm showEdit" data-id="$1" title="Ubah" ><i class="glyphicon glyphicon-pencil"></i></button>&nbsp;<button class="btn btn-danger btn-sm showDelete" data-id="$1" title="Hapus"><i class="glyphicon glyphicon-trash"></i></button>', 'id');
        return $this->datatables->generate();
    }

    public function user_get_by_id($id){
        return $this->db->get_where('users', array('id' => $id))->result();
    }

    public function get_list_data(){
        return $this->db->get('users')->result();
    }

    /*public function insert($data){
        $this->db->trans_start();

        $this->db->insert('users', $data);

        $this->db->trans_complete();

        if($this->db->affected_rows() > 0){
            return true;
        }
        else{
            if($this->db->trans_status() === false){
                return false;
            }
            else{
                return true;
            }
        }
    }

    public function update($data, $id){
        $this->db->trans_start();
        if($this->check_duplicate($data["id"]) != 1){
            $this->db->where('id', $id);
            $this->db->update('users', $data);
        }
        else{
            return false;
        }
        $this->db->trans_complete();

        if($this->db->affected_rows() > 0){
            return true;
        }
        else{
            if($this->db->trans_status() === false){
                return false;
            }
            else{
                return true;
            }
        }
    }

    public function check_duplicate($id){
        return $this->db->where('id', $id)->from("users")->count_all_results();
    }

    public function delete($id){
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete('users');
        $this->db->trans_complete();

        if($this->db->affected_rows() > 0){
            return true;
        }
        else{
            if($this->db->trans_status() === false){
                return false;
            }
            else{
                return true;
            }
        }
    }*/

}