<?php
class Lihatkegiatanluar extends CI_Model {

    public function __construct()
    {
        parent::__construct();
                // Your own constructor code
    }

    public function lihatkegiatanluar_get_all(){

        $this->datatables->select('id_kegiatanluar, id_ksluar, tanggal, uraian, persentase, file');
        $this->datatables->from('kegiatan_luar');
        $this->datatables->add_column('Aksi', '<button class="btn btn-primary btn-sm showEdit" data-id="$1" title="Ubah" ><i class="glyphicon glyphicon-pencil"></i></button>&nbsp;<button class="btn btn-danger btn-sm showDelete" data-id="$1" title="Hapus"><i class="glyphicon glyphicon-trash"></i></button>', 'id_ksluar');
        return $this->datatables->generate();
    }

    public function lihatkegiatanluar_get_all_by_id($id){

        // $this->datatables->select('id_kegiatanluar, id_ksluar, tanggal, uraian, persentase, file');
        // $this->datatables->from('kegiatan_luar');
        // $this->datatables->where('id_ksluar', $id);
        // $this->datatables->add_column('Aksi', '<button class="btn btn-primary btn-sm showEdit" data-id="$1" title="Ubah" ><i class="glyphicon glyphicon-pencil"></i></button>&nbsp;<button class="btn btn-danger btn-sm showDelete" data-id="$1" title="Hapus"><i class="glyphicon glyphicon-trash"></i></button>', 'id_kegiatanluar');
        // return $this->datatables->generate();
        $this->db->select('id_kegiatanluar, id_ksluar, tanggal, uraian, persentase, file');
        $this->db->from('kegiatan_luar');
        $this->db->where('id_ksluar', $id);
        $this->db->order_by('id_ksluar', 'DESC');
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function lihat_kegiatan_luar_by_id($id){
        return $this->db->get_where('kegiatan_luar', array('id_kegiatanluar' => $id))->result();
    }

    public function lihatkegiatanluar_get_by_id($id_ksluar){
        return $this->db->get_where('kegiatan_luar', array('id_ksluar' => $id_ksluar))->result();
    }

    public function update_kegiatan($data_kegiatan_lihat, $id){

        $this->db->where('id_kegiatanluar', $id);
        return $this->db->update('kegiatan_luar', $data_kegiatan_lihat);

    }

    public function check_duplicate($id_kegiatanluar){
        return $this->db->where('id_kegiatanluar', $id_kegiatanluar)->from("kegiatan_luar")->count_all_results();
    }

    public function insert($data){
        $this->db->trans_start();
            //if($this->check_duplicate($data["id_ksdalam"]) != 1){
        $this->db->insert('kegiatan_luar', $data);
            /*}
            else{
                return false;
            }*/
            $this->db->trans_complete();

            if($this->db->affected_rows() > 0){
                return true;
            }
            else{
                if($this->db->trans_status() === false){
                    return false;
                }
                else{
                    return true;
                }
            }
        }

        public function delete_kegiatan($id_kegiatanluar){
            $this->db->trans_start();
            $this->db->where('id_kegiatanluar', $id_kegiatanluar);
            $this->db->delete('kegiatan_luar');
            $this->db->trans_complete();

            if($this->db->affected_rows() > 0){
                return true;
            }
            else{
                if($this->db->trans_status() === false){
                    return false;
                }
                else{
                    return true;
                }
            }
        }

        public function tambah_kegiatan($data){
            return $this->db->insert('kegiatan_luar', $data);
        }
    }