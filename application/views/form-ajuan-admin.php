<?php 
$this->load->view('header.php');
?>

<!-- COUNTERS -->
<section class="text-light background-overlay-dark" style="padding: 40px 0 !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3>Data Formulir Ajuan Universitas YARSI</h3>
                    <div class="seperator seperator-small"></div>
                    <h4>Data Form Ajuan</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: COUNTERS -->

<!-- Page Content -->
<section id="page-content" class="no-sidebar">
    <div class="container">
        <!-- DataTable -->
        <div class="row mb-3">
            <div class="col-lg-6">
                <h4>Data Formulir Ajuan</h4>
                <p>Dashboard Ajuan Kerjasama</p>
            </div>
            <div class="col-lg-6 text-right">
                <div id="export_buttons" class="mt-2"></div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table id="datatable" class="table table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pengusul</th>
                            <th>Dokumen Ajuan</th>
                            <th>Tanggal Ajuan</th>
                            <th>Reviewer</th>
                            <th>Catatan</th>
                            <th>Validasi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; foreach ($ajuan as $a) : ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php $user = $this->db->get_where('users', array('id' => $a['id_user']))->result_array(); if(!empty($user)){ echo $user[0]['first_name']." ".$user[0]['last_name']; }?></td>
                            <td>
                                <a href="<?php echo base_url('./uploads/ajuan/').$a['dokumen']; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/images/msword.png') ?>" width="40"/>
                                </a>
                                <?php if ($a['jenis_ajuan'] == "Perbaikan Ajuan"){
                                    echo $a['jenis_ajuan'];
                                } ?>
                            </td>
                            <td><?php if ($a['status'] == 'Diterima') {
                                $color = '#b8f9bf';
                            }elseif ($a['status'] == 'Ditolak') {
                                $color = '#fcd4d4';
                            }elseif ($a['status'] == 'Perbaiki'){
                                $color = '#FFE700';
                            }else{
                                $color = '#00FFFF';
                            } $tgl = explode(" ", $a['tgl_ajuan']); echo date_indo($tgl[0]); ?></td>
                            <td><?php echo $a['reviewer']; ?></td>
                            <td><?php echo $a['catatan']; ?></td>
                            <td><a <?= ($a['status'] == 'Diproses') ? 'data-target="#modal" data-toggle="modal" style="cursor: pointer;"' : '' ?> class="mod" data-id="<?php echo $a['id']; ?>"><span class="badge badge-pill" style="background-color: <?php echo $color; ?>"><?php echo $a['status']; ?></span></a>
                        <?php 
                        if($a['id_kat'] == 1){
                            $r = $this->db->get_where('ks_dalam', array('id_ajuan' => $a['id']))->row();
                        }else{
                            $r = $this->db->get_where('ks_luar', array('id_ajuan' => $a['id']))->row();
                        }
                          
                        if ($a['status'] == 'Diterima') { 
                            if (empty($r)) { ?>
                            <a data-target="#modal-kerjasama" data-toggle="modal" href="#" class="mod" data-id="<?php echo $a['id']; ?>"><span class="badge badge-pill" style="background-color: #71c6f7;" data-id="<?php echo $a['id']; ?>" data-kat="<?php echo $a['id_kat']; ?>" data-kerma="<?php echo $a['id_kerma']; ?>">Tambah Kerjasama</span></a>   
                        <?php } } ?>    
                        </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- end: DataTable -->
</div>
</section>
<!-- end: Page Content -->


<!--Modal Validasi-->
<div class="modal fade" id="modal" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true"
style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="modal-label">Validasi Ajuan</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                 <form method="POST" action="<?php echo base_url('Form/update_ajuan'); ?>" id="" enctype="multipart/form-data">
                    <input type="hidden" name="id" class="id" value="">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Status Ajuan</label>
                        <select class="form-control" required="" name="status" id="status">
                            <option value="">--Silahkan Pilih--</option>
                            <option value="Diterima">Diterima</option>
                            <option value="Perbaiki">Perbaiki</option>
                            <option value="Ditolak">Ditolak</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tulis Review</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" name="catatan" rows="6" required="required"></textarea>
                    </div>
                    <div id="seleksi">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kategori Kerjasama</label>
                            <select class="form-control" name="id_kategori">
                                <option value="">--Silahkan Pilih--</option>
                                <option value="1">Pemerintah</option>
                                <option value="2">Institusi Pendidikan</option>
                                <option value="3">Dunia Kerja</option>
                                <option value="4">Organisasi</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Jenis Kerjasama</label>
                            <select class="form-control" name="id_kerma">
                                <option value="">--Silahkan Pilih--</option>
                                <option value="1">Dalam Negeri</option>
                                <option value="2">Luar Negeri</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group pull-right"><button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button></div> -->
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-b" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-b">Simpan</button>
        </div>
    </form>
</div>
</div>
</div>
<!-- end: Modal -->
<?php 
$this->load->view('modal-tambah-kerjasama.php');
?>
<?php 
$this->load->view('footer.php');
?>
<!-- Js for modal ajuan -->
<script type="text/javascript">
$(document).on("click", ".badge", function () {
    var nilkerma = $(this).attr("data-kerma");
    $('#pem').val($(this).attr("data-id"));
    if (nilkerma == '1'){
        var txt = 'Dalam Negeri'; 
        $('#form').attr('action','<?php echo base_url('Formulir/insertdalam') ?>');
        // $('#id_kat').val(nilkerma);
    }
    else {
        var txt = 'Luar Negeri'; 
        $('#form').attr('action','<?php echo base_url('Formulir/insertluar') ?>');
        // $('#id_kat').val(nilkerma);
    }
    var nilkat = $(this).attr("data-kat");
    $('#id_kat').val(nilkat);
    if(nilkat == '1'){var txtt = 'Pemerintah';}
    else if(nilkat == '2'){var txtt = 'Institusi Pendidikan';}
    else if(nilkat == '3'){var txtt = 'Dunia Usaha';}
    else if(nilkat == '4'){var txtt = 'Organisasi';}

    $('#pem').val(txt);
    $('#dal').val(txtt);
    $('#id_aju').val($(this).attr("data-id"));
    var option = '<option value="" disabled>--Silahkan Pilih--</option>';
    $.ajax({url: '<?php echo base_url("Pihakinternal/listData"); ?>',
        type: 'GET',
        dataType: 'json',
        success: function(result){
            result.forEach(function (item){
                option += '<option value="' + item.nama_internal + '">' + item.nama_internal + '</option>';
            });
            $(".modal-body #nama_internal").html(option);
        }
    });
});
</script>
<!-- Export Datatable -->
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable( {
            buttons: [
            {
                extend: 'print',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }

            },{
                extend: 'pdf',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            },{
                extend: 'excel',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }, {
                extend: 'copy',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }
            ]
        } );

        table.buttons().container().appendTo( '#export_buttons' );
        $("#export_buttons .btn").removeClass('btn-secondary').addClass('btn-light');
    } );

    $(document).on("click", ".mod", function () {
        $("#modal .modal-body .id").val($(this).attr("data-id"));
    });

</script>

<script>
    $("#seleksi").hide();
    $('#status').on('change',function(){ 
      var value = $(this).val();
      if (value == 'Diterima') {
        $("#seleksi").show();
    }else{
        $("#seleksi").hide();
    }
});
</script>

