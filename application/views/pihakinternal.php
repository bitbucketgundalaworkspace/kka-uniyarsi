<?php 
$this->load->view('header.php');
?>

<!-- COUNTERS -->
<section style="padding: 40px 0 !important; background-color: #fec51c !important">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3>Data Pihak Internal</h3>
                    <div class="seperator seperator-small"></div>
                    <h4>Halaman Admin</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: COUNTERS -->

<!-- Page Content -->
<section id="page-content" class="no-sidebar">
    <div class="container">
        <!-- DataTable -->
        <div class="row mb-3">
            <div class="col-lg-6">
                <h4>Data Pihak Internal</h4>
            </div>
            <div class="col-lg-6 text-right">
                <button type="button" class="btn btn-light showAdd" style="background-color: #fec51c" href="<?php echo base_url('auth/create_user');?>"><i class="fa fa-plus"></i> Tambah Data</button>
                <div id="export_buttons" class="mt-2"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table id="example" class="table table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pihak Internal</th>
                            <th class="noExport">Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- end: DataTable -->

    </div>
</section>
<!-- end: Page Content -->

<?php 
$this->load->view('footer.php');
?>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog" >
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="overflow: auto;padding-bottom: 15px;">
            <div class="modal-header">
                <h4 class="modal-title" id="formAddEditTitle">Form Edit</h4>
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            </div>
            <div class="modal-body">

                <form method="POST" action="#" id="formAddEdit">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Pihak Internal</label>
                        <input type="text" class="form-control" name="nama_internal" placeholder="Nama Internal" value="" id="nama_internal" required="required">
                    </div>
                    <input type="hidden" name="id" value="" id="id">
                    <div class="form-group pull-right"><button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button></div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalDelete" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="overflow: auto;padding-bottom: 15px;">
            <div class="modal-header">
                <h4 class="modal-title" id="titleHapus">Hapus Data?</h4>
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            </div>
            <div class="modal-body">
                <div class="pull-right"><a type="button" class="btn btn-danger" id="deleteButton" href="#">Ya</a>&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" id="cancelDeleteButton">Batal</button></div>
            </div>
        </div>
    </div>
</div>
<!--End Modal Delete-->

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#example").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#example_filter input')
                .off('.DT')
                .on('keyup.DT', function(e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            scrollX: true,
            bAutoWidth: true,
            processing: true,
            serverSide: true,
            ajax: {"url": "pihakinternal/data", "type": "POST"},
            columns: [
            {
                "data": "nama_internal",
                "orderable": false
            },
            {"data": "nama_internal"},
            {"data": "Aksi"}
            ],
            order: [[1, 'asc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });

    $(document).on("click", ".showAdd", function () {
        uri = '<?php echo base_url("pihakinternal/insert");?>';
        $("#formAddEditTitle").html('Form Tambah Pihak Internal');
        $(".modal-body #nama_internal").val( '' );
        $("#formAddEdit").attr('action', uri);
        $('#myModal').modal('show');
    });

    $(document).on("click", ".showEdit", function () {
        uri = '<?php echo base_url("pihakinternal/update");?>'; 
        url = '<?php echo base_url("pihakinternal/data");?>'; 
        var id = $(this).data('id');
        $.ajax({ url: url + '/' + id, 
            type: 'GET', 
            dataType: 'json', 
            success: function(result) { 
                var j = 1;
                console.log(result[0]);
                for (var i = 0; i < result.length; i++){
                    $("#formAddEditTitle").html('Form Edit Nama Internal');
                    $(".modal-body #nama_internal").val( result[i].nama_internal );
                    $(".modal-body #id").val( id );
                    $("#formAddEdit").attr('action', uri);
                    $('#myModal').modal('show');
                }
            }
        }); 
    });

    $("#formAddEdit").submit(function(e){
        e.preventDefault();
        $.post(uri, $("#formAddEdit").serialize(), function(data) {
            if(data == "true"){
                 Swal.fire({
              position: 'center',
              type: 'success',
              title: 'Berhasil Disimpan',
              showConfirmButton: false,
              timer: 1500
            });
                // $("#example").dataTable().fnDraw();
                $('#myModal').modal('hide');
                window.location.replace("<?php echo base_url('Pihakinternal'); ?>");
            }
            else{
                Swal.fire({
                  position: 'center',
                  type: 'error',
                  title: 'Gagal Disimpan !',
                  showConfirmButton: false,
                  timer: 1500
                });
                $('#myModal').modal('hide');
                window.location.replace("<?php echo base_url('Pihakinternal'); ?>");
            }
        });
    });

    $(document).on("click", ".showDelete", function () {
        url = '<?php echo base_url("pihakinternal/delete");?>'; 
        var id = $(this).data('id');
        $("#titleHapus").html("Anda yakin ingin menghapus data ini?");
        $('#myModalDelete').modal('show');

        $("#deleteButton").click(function(){
            $.post( url, { id : id }, function( data ) {
                if(data == "true"){
                    alert("Data berhasil di hapus");
                    $("#example").dataTable().fnDraw();
                    $('#myModalDelete').modal('hide');
                }
                else{
                    alert("Data gagal di hapus"); 
                }
            });
        });
    });

</script>  

<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable( {
            buttons: [
            {
                extend: 'print',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }

            },{
                extend: 'pdf',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            },{
                extend: 'excel',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }, {
                extend: 'copy',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }
            ]
        } );

        table.buttons().container().appendTo( '#export_buttons' );

        $("#export_buttons .btn").removeClass('btn-secondary').addClass('btn-light');
    } );
</script>
