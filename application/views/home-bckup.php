<?php 
$this->load->view('header.php');
?>

<!-- Inspiro Slider -->
<div id="slider" class="inspiro-slider slider-halfscreen arrows-large arrows-creative dots-creative" data-height-xs="360" data-autoplay-timeout="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <!-- Slide 1 -->
    <div class="slide background-image" style="background-image:url('<?php echo base_url();?>assets/homepages/corporate-v6/images/5.jpg');">
        <div class="container">
            <div class="slide-captions text-center">
                <!-- Captions -->
                <h2 class="text-lg m-b-0 text-dark">Pusat Kerja Sama</h2>
                <h2 class="text-medium text-dark">dan Hubungan Internasional</h2>
                <a class="btn btn-dark btn-outline" href="#">Kerjasama</a>
                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <!-- end: Slide 1 -->
    <!-- Slide 2 -->
    <div class="slide background-image" style="background-image:url('<?php echo base_url();?>assets/homepages/corporate-v6/images/6.jpg');">
        <div class="container">
            <div class="slide-captions text-left">
                <!-- Captions -->
                <h2 class="text-lg m-b-0 text-dark">Pusat Kerja Sama</h2>
                <h2 class="text-medium text-dark">dan Hubungan Internasional</h2>
                <a class="btn btn-dark btn-outline" href="#">Kerjasama</a>
                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <!-- end: Slide 2 -->
</div>
<!--end: Inspiro Slider -->

<!-- About us -->
<section id="section-about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Bidang Kerjasama YARSI</h1>
                <div class="row">
                    <!-- features box -->
                    <div class="col-lg-6">
                        <p>We’re POLO, a creative agency located in the heart of New York city. A true story, that never been told!. Fusce id mi diam, non ornare orci. Pellentesque ipsum erat, facilisis ut venenatis eu.</p>
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. A true story, that never been told!. Fusce id mi diam, non ornare. Fusce id mi diam, non ornare orci. Pellentesque ipsum erat, facilisis ut venenatis eu.</p>
                        <a href="#services" class="btn btn-rounded">More info</a>
                    </div>
                    <!-- end: features box -->

                    <!-- Progress bar -->
                    <div class="col-lg-6">
                        <div class="p-progress-bar-container title-up small color">
                            <div class="p-progress-bar" data-percent="100" data-delay="0" data-type="%">
                                <div class="progress-title"><h6>Target Kerja Sama Luar Negeri 2019</h6></div>
                            </div>
                        </div>

                        <div class="p-progress-bar-container title-up small color">
                            <div class="p-progress-bar" data-percent="94" data-delay="100" data-type="%">
                                <div class="progress-title"><h6>Target Kerja Sama Dalam Negeri 2019</h6></div>
                            </div>
                        </div>

                        <div class="p-progress-bar-container title-up small color">
                            <div class="p-progress-bar" data-percent="89" data-delay="200" data-type="%">
                                <div class="progress-title"><h6>Target Produk Terhilirisasi 2019</h6></div>
                            </div>
                        </div>
                    </div>
                    <!-- end: Progress bar -->
                </div>
            </div>
            <!-- end features box -->
        </div>
    </div>
</section>
<!-- end: About us -->

<!-- SERVICES -->
<section class="background-grey">
    <div class="container">
        <div class="row">
            <div style="margin-bottom: 20px">
                <h1>Layanan Kami</h1>
                <p>Universitas YARSI dipenuhi oleh banyak pakar dan peneliti yang dapat mengembangkan bisnis dan memberikan saran dalam banyak bidang</p>
            </div>
            <!--Box 1-->
            <div class="row col-no-margin">    
                <div class="col-lg-3">
                    <div class="text-box hover-effect text-dark">
                        <h3>Kerjasama Internasional </h3>
                        <p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>
                    </a>
                </div>
            </div>
            <!--End: Box 1-->
            <!--Box 2-->
            <div class="col-lg-3">
                <div class="text-box hover-effect text-dark">
                    <h3>Kerjasama Nasional </h3>
                    <p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>
                </a>
            </div>
        </div>
        <!--End: Box 2-->
        <!--Box 3-->
        <div class="col-lg-3">
            <div class="text-box hover-effect text-dark">
                <h3>Hilirisasi <br>Produk</h3>
                <p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>
            </a>
        </div>
    </div>
    <!--End: Box 3-->
    <!--Box 4-->
    <div class="col-lg-3">
        <div class="text-box hover-effect text-dark">
            <h3>Korporasi Akademik</h3>
            <p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>
        </a>
    </div>
</div>
<!--End: Box 4-->
</div>
</div>
</section>
<!-- end: SERVICES -->

<!-- COUNTERS -->
<section class="text-light background-overlay-dark" data-parallax-image="images/parallax/5.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="text-center">
                    <div class="counter"> <span data-speed="3000" data-refresh-interval="50" data-to="12416"
                        data-from="600" data-seperator="true"></span> </div>
                        <div class="seperator seperator-small"></div>
                        <h5>Kerjasama Luar Negeri</h5>
                    </div>
                </div>

                <div class="col-lg-6">

                    <div class="text-center">
                        <div class="counter"> <span data-speed="4500" data-refresh-interval="23" data-to="365"
                            data-from="100" data-seperator="true"></span> </div>
                            <div class="seperator seperator-small"></div>
                            <h5>Kerjasama Dalam Negeri</h5>
                        </div>
                    </div>

                    </div>
                </div>
            </section>
            <!-- end: COUNTERS -->

            <section>
                <div class="container">
                   <!--Team members shadow-->
                   <div class="text-center" style="margin-bottom: 20px">
                    <h1>Tim Kami</h1> 
                </div>
                <div class="carousel team-members team-members-shadow" data-arrows="false" data-margin="20" data-items="3">

                    <div class="team-member">
                        <div class="team-image">
                            <img src="<?php echo base_url();?>assets/images/team/6.jpg">
                        </div>
                        <div class="team-desc">
                            <h3>Alea Smith</h3>
                            <span>Software Developer</span>
                        </div>
                    </div>

                    <div class="team-member">
                        <div class="team-image">
                            <img src="<?php echo base_url();?>assets/images/team/6.jpg">
                        </div>
                        <div class="team-desc">
                            <h3>Ariol Doe</h3>
                            <span>Software Developer</span>
                        </div>
                    </div>

                    <div class="team-member">
                        <div class="team-image">
                            <img src="<?php echo base_url();?>assets/images/team/6.jpg">
                        </div>
                        <div class="team-desc">
                            <h3>Emma Ross</h3>
                            <span>Software Developer</span>
                        </div>
                    </div>

                </div>
                <!--END: Team members shadow-->
            </div>
        </section>

        <!-- Testimonial Carousel -->
        <section class="background-grey">
            <div class="container">
                <div class="text-center" style="margin-bottom: 20px">
                    <h1>Apa yang Orang Katakan Tentang Kami</h1> 
                </div>
                <!-- Testimonials -->
                <div class="carousel equalize testimonial testimonial-box" data-margin="20" data-arrows="false" data-items="3" data-items-sm="2" data-items-xxs="1" data-equalize-item=".testimonial-item">

                    <!-- Testimonials item -->
                    <div class="testimonial-item">
                        <img src="<?php echo base_url();?>assets/images/team/6.jpg" alt="">
                        <p>Polo is by far the most amazing template out there! I literally could not be happier that I chose to buy this template!</p>
                        <span>Alan Monre</span>
                        <span>CEO, Square Software</span>
                    </div>
                    <!-- end: Testimonials item-->

                    <!-- Testimonials item -->
                    <div class="testimonial-item">
                        <img src="<?php echo base_url();?>assets/images/team/6.jpg" alt="">
                        <p>Polo is by far the most amazing template out there! I literally could not be happier that I chose to buy this template!</p>
                        <span>Alan Monre</span>
                        <span>CEO, Square Software</span>
                    </div>
                    <!-- end: Testimonials item-->

                    <!-- Testimonials item -->
                    <div class="testimonial-item">
                        <img src="<?php echo base_url();?>assets/images/team/6.jpg" alt="">
                        <p>The world is a dangerous place to live; not because of the people who are evil, but because of the people who don't do anything about it.</p>
                        <span>Alan Monre</span>
                        <span>CEO, Square Software</span>
                    </div>
                    <!-- end: Testimonials item-->

                    <!-- Testimonials item -->
                    <div class="testimonial-item">
                        <img src="<?php echo base_url();?>assets/images/team/6.jpg" alt="">
                        <p>Art is the only serious thing in the world. And the artist is the only person who is never serious.</p>
                        <span>Resa Smith</span>
                        <span>Developer @Apple</span>
                    </div>
                    <!-- end: Testimonials item-->

                    <!-- Testimonials item -->
                    <div class="testimonial-item">
                        <img src="<?php echo base_url();?>assets/images/team/6.jpg" alt="">
                        <p>The world is a dangerous place to live; not because of the people who are evil, but because of the people who don't do anything about it.</p>
                        <span>Ariol Perry</span>
                        <span>Creative Designer</span>
                    </div>
                    <!-- end: Testimonials item-->

                </div>
                <!-- end: Testimonials -->
            </div>
        </section>
        <!-- end: Testimonial Carousel -->

        <!-- CLIENTS -->
        <!-- <section>
            <div class="container">
                <div class="text-center">
                    <h1>Mitra Kami</h1>
                    <span class="lead">Mitra luar biasa kami, kami senang bekerja sama!</span>
                </div>

                <ul class="grid grid-5-columns">
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/clients/1.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/clients/2.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/clients/3.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/clients/4.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/clients/5.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/clients/6.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/clients/7.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/clients/8.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/clients/9.png" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/clients/10.png" alt="">
                        </a>
                    </li>
                </ul>

            </div>
        </section> -->
        <!-- CLIENTS -->

        <!-- BLOG -->
        <!-- <section class="background-grey">
            <div class="container">
                <div data-animate-delay="100" data-animate="fadeInUp">
                    <div class="heading-text heading-section text-center">
                        <h1 class="text-medium">Berita Terbaru</h1>
                    </div>
                </div>
                <div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
            
                    <div class="post-item border">
                        <div class="post-item-wrap">
                            <div class="post-image">
                                <a href="#">
                                    <img alt="" src="<?php echo base_url();?>assets/images/blog/1.jpg">
                                </a>
                            </div>
                            <div class="post-item-description">
                                <span class="post-meta-date"><i class="fa fa-calendar-o"></i>Jan 21, 2017</span>
                                <h2><a href="#">Standard post with a single image
                                </a></h2>
                                <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>
                                <a href="#" class="item-link">Selengkapnya <i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </section> -->
        <!-- BLOG -->
<?php 
    $this->load->view('footer.php');
?>      


