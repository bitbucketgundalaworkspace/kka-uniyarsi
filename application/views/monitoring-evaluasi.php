<?php 
$this->load->view('header.php');
?>

<!-- COUNTERS -->
<section class="text-light background-overlay-dark" style="padding: 40px 0 !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3>Monitoring dan Evaluasi Kerja Sama Universitas YARSI</h3>
                    <div class="seperator seperator-small"></div>
                    <h4>Monev Kerja Sama</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: COUNTERS -->

<!-- Page Content -->
<section id="page-content" class="no-sidebar">
    <div class="container">
        <!-- DataTable -->
        <div class="row mb-3">
            <div class="col-lg-6">
                <h4>Data Monitoring dan Evaluasi Kerja Sama</h4>
                <p>Dashboard Monev Kerjasama</p>
            </div>
            <div class="col-lg-6 text-right">
                <button data-target="#modal-upload" data-toggle="modal" href="#" type="button" class="btn btn-light showAdd" style="background-color: #fec51c"><i class="fa fa-plus"></i> Upload Monev</button>
                <!-- Buat Admin Kasih Catatan -->
                <!-- <button data-target="#modal-catatan" data-toggle="modal" href="#" type="button" class="btn btn-light showAdd" style="background-color: #fec51c"><i class="fa fa-plus"></i> Kirim Catatan</button> -->
                <div id="export_buttons" class="mt-2"></div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table id="datatable" class="table table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Jenis Monev</th>
                            <th>Dokumen Monev</th>
                            <th>Tanggal Monev</th>
                            <th>Catatan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; foreach ($monev as $a) : 
                        $query = $this->db->get_where('users', array('id' => $this->session->userdata('user_id')))->result_array();
                        //if($d['nama_internal'] == $query[0]['pihak_internal'] && $this->session->userdata('group_user') == 3) {
                            ?>
                          
                        <?php /*} else */if($this->session->userdata('group_user') == 1 || $this->session->userdata('group_user') == 3) { ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $a['jenis_monev']; ?></td>
                                <td>
                                    <a href="<?php echo base_url('./uploads/monev/').$a['dokumen']; ?>" target="_blank">
                                        <img src="<?php echo base_url('assets/images/msword.png') ?>" width="40"/>
                                    </a>
                                </td>
                                <td><?php $tgl = explode(" ", $a['tgl_monev']); echo date_indo($tgl[0]); ?></td>
                                <td><?php echo $a['catatan']; ?></td>
                                <td>
                                    <?php if ($this->session->userdata('group_user') == 1) { ?>
                                        <!-- Buat Superadmin -->
                                        <a class="ml-2 myModal2" data-target="#modal-catatan" data-toggle="modal" href="#" data-toggle="tooltip" data-original-title="Kirim Catatan" data-idd="<?php echo $a['id']; ?>"><i class="fa fa-plus"></i></a>
                                    <?php } ?>
                                    <?php if ($this->session->userdata('group_user') == 3) { ?>
                                        <!-- Buat Operator -->
                                        <a class="ml-2 myModalDelete showDelete" data-idd="<?php echo $a['id']; ?>" data-target="#modal-hap" data-toggle="modal" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DataTable -->
    </div>
</section>
<!-- end: Page Content -->


<!--Modal Upload Monev-->
<div class="modal fade" id="modal-upload" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true"
style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="modal-label">Upload Monev </h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">

            <div class="row">
                <div class="col-md-12">
                    <form method="POST" action="<?php echo base_url('Monev/upload'); ?>" id="" enctype="multipart/form-data">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Jenis Monev </label>
                           <select class="form-control" required="" name="jenis">
                            <option value="">--Silahkan Pilih--</option>
                            <option value="Monev Akhir Tahun">Monev Akhir Tahun</option>
                            <option value="Monev Akhir Periode">Monev Akhir Periode</option>
                        </select>
                        <input type="hidden" name="ks" value="<?php echo $this->uri->segment('3'); ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Upload Dokumen</label>
                        <input type="file" class="form-control" id="file_ajuan" name="file_ajuan" value="">
                    </div>
                    <?php if($this->uri->segment('1') == 'dalamnegeri'){ ?> 
                        <input type="hidden" name="jns" value="1">
                    <?php }else{ ?>
                        <input type="hidden" name="jns" value="2">
                    <?php } ?>
                    

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-b" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-b">Simpan</button>
        </div>
    </form>
</div>
</div>
</div>
<!-- end: Modal -->

<!--Modal Catatan-->
<div class="modal fade" id="modal-catatan" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true"
style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="modal-label">Catatan Monev </h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">

            <div class="row">
                <div class="col-md-12">
                   <form method="POST" action="<?php echo base_url('Monev/monevcomen'); ?>" id="" enctype="multipart/form-data">
                    <!-- <div class="form-group">
                     <input type="hidden" name="ajuan" value="Ajuan Baru">
                     <label for="exampleInputEmail1">Jenis Monev</label>
                     <select class="form-control" required="" name="jenis">
                        <option value="">--Silahkan Pilih--</option>
                        <option value="1">Monev Akhir Tahun</option>
                        <option value="2">Monev Akhir Periode</option>
                    </select>
                </div> -->
                <input class="hid" type="hidden" name='id'>
                <input type="hidden" name="ks" value="<?php echo $this->uri->segment('3'); ?>">
                <input type="hidden" name="direct" value="<?php echo $this->uri->segment('1'); ?>">
                <div class="form-group">
                    <label for="exampleInputEmail1">Catatan Monev</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" name="catatan" rows="6" required="required"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-b" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-b">Simpan</button>
    </div>
</form>
</div>
</div>
</div>
<!-- end: Modal -->

<!--Modal Catatan-->
<div class="modal fade" id="modal-hap" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true"
style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="modal-label">Hapus Monev ?</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">

            <div class="row">
                <div class="col-md-12">
                   <form method="POST" action="<?php echo base_url('Monev/monevdelete'); ?>" id="" enctype="multipart/form-data">
                    <input class="hidd" type="hidden" name='id'>
                    <input type="hidden" name="ks" value="<?php echo $this->uri->segment('3'); ?>">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-b" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-b">Simpan</button>
        </div>
    </form>
</div>
</div>
</div>
<!-- end: Modal -->

<?php 
$this->load->view('modal-tambah-kerjasama.php');
?>

<?php 
$this->load->view('footer.php');
?>

<script type="text/javascript">
    $(document).on("click", ".myModal2", function () {
        $('.hid').val($(this).attr("data-idd"));
    });

    $(document).on("click", ".myModalDelete", function () {
    // alert($(this).attr("data-idd"));
    $('.hidd').val($(this).attr("data-idd"));
});

    $(document).on("click", ".badge", function () {
        var nilkat = $(this).attr("data-kat");
        if (nilkat == '1'){var txt = 'Dalam Negeri'; $('#form').attr('action','<?php echo base_url('Formulir/insertdalam') ?>');}
        else {var txt = 'Luar Negeri'; $('#form').attr('action','<?php echo base_url('Formulir/insertluar') ?>');}
        var nilker = $(this).attr("data-kerma");
        if(nilker == '1'){var txtt = 'Pemerintah';}
        else if(nilker == '2'){var txtt = 'Institusi';}
        else if(nilker == '3'){var txtt = 'Dunia Usaha';}
        else if(nilker == '4'){var txtt = 'Organisasi';}

        $('#pem').val(txt);
        $('#dal').val(txtt);
        $('#id_kat').val($(this).attr("data-kat"));
        var option = '<option value="" disabled>--Silahkan Pilih--</option>';
        $.ajax({url: '<?php echo base_url("Pihakinternal/listData"); ?>',
            type: 'GET',
            dataType: 'json',
            success: function(result){
                result.forEach(function (item){
                    option += '<option value="' + item.nama_internal + '">' + item.nama_internal + '</option>';
                });
                $(".modal-body #nama_internal").append(option);
            }
        });
    });
</script>
<!-- Export Datatable -->
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable( {
            buttons: [
            {
                extend: 'print',
                title: 'Berkas Ajuan',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }

            },{
                extend: 'pdf',
                title: 'Berkas Ajuan',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            },{
                extend: 'excel',
                title: 'Berkas Ajuan',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }, {
                extend: 'copy',
                title: 'Berkas Ajuan',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }
            ]
        } );

        table.buttons().container().appendTo( '#export_buttons' );
        $("#export_buttons .btn").removeClass('btn-secondary').addClass('btn-light');
    } );

    $(document).on("click", ".mod", function () {
        $('#spn').text($(this).attr("data-status"));
        $('#catat').text($(this).attr("data-catatan"));
        if($(this).attr("data-status") != null){
            $('.cat').show();
            $('#menunggu').hide();
        }
        if($(this).attr("data-status") == 'Diproses'){
            $('#menunggu').show();
            $('.cat').hide();    
        }
        if($(this).attr("data-status") == 'Perbaiki'){
            $('.sta').show();
        }else{
            $('.sta').hide();    
        }
        $("#modal .modal-body .id").val($(this).attr("data-id"));
    });
</script>

