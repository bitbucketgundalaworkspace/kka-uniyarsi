<!-- Footer -->
<footer id="footer">
    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="widget">
                        <div id="logo">
                            <a href="<?php echo base_url('');?>" class="logo" data-src-dark="<?php echo base_url();?>assets/images/sikerma-pth.png">
                                <img src="<?php echo base_url();?>assets/images/sikerma-pth.png" alt="YARSI Logo" style="max-width: 300px">
                            </a>
                        </div>
                        <hr>
                        <p class="mb-5 text-light">Jl. Letjen. Suprapto, RT. 10 / RW. 5 Cemp. Putih Tim.,<br> Cemp. Putih, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10410</p>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="widget">
                                <div class="widget-title text-light">Halaman Website</div>
                                <ul class="list text-light">
                                    <li><a href="<?php echo base_url('form/ajuan');?>">Sikerma</a></li>
                                    <li><a href="<?php echo base_url('dalamnegeri?idlm=1');?>">Kerjasama</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="widget">
                                <div class="widget-title text-light">Sistem Informasi</div>
                                <ul class="list text-light text-light">
                                    <li><a href="https://www.yarsi.ac.id" target="_blank">Universitas YARSI</a></li>
                                    <li><a href="http://sisakad.yarsi.ac.id" target="_blank">Sistem Akademik</a></li>
                                    <li><a href="http://medinfo.yarsi.ac.id" target="_blank">Medinfo</a></li>
                                    <li><a href="http://kuliah.yarsi.ac.id" target="_blank">E-learning non - FK</a></li>
                                    <li><a href="http://elearning.yarsi.ac.id" target="_blank">E-learning FK</a></li>
                                    <li><a href="http://perpustakaan.yarsi.ac.id" target="_blank">Perpustakaan</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="widget">
                                <div class="widget-title text-light">Pusat Bantuan</div>
                                <ul class="list text-light">
                                    <li><a href="#">Bantuan</a></li>
                                    <li><a href="#">Hubungi Kami</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="copyright-content">
        <div class="container">
            <div class="copyright-text text-center" style="color: #000000">&copy; 2019 Universitas YARSI -  All Right Reserved</div>
        </div>
    </div>
</footer>
<!-- end: Footer -->

</div>
<!-- end: Body Inner -->

<!-- Scroll top -->
<a id="scrollTop"><i class="icon-chevron-up1"></i><i class="icon-chevron-up1"></i></a>
<!--Plugins-->
<script src="<?php echo base_url();?>assets/js/jquery.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>

<!--Template functions-->
<script src="<?php echo base_url();?>assets/js/functions.js"></script>

<!-- Datatables component-->
<script src='<?php echo base_url();?>assets/js/plugins/components/datatables/datatables.min.js'></script>

<!-- Swall -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript">
    <?php if($this->session->userdata('msg') == TRUE){ ?>
        $(document).ready(function () {
            Swal.fire({
              position: 'center',
              type: 'success',
              title: 'Berhasil Login',
              showConfirmButton: false,
              timer: 1500
          });
        });
    <?php } ?>

    <?php if($this->session->userdata('success') == TRUE){ ?>
        $(document).ready(function () {
            Swal.fire({
              position: 'center',
              type: 'success',
              title: 'Data Berhasil Disimpan !',
              showConfirmButton: false,
              timer: 1500
          });
        });
    <?php } else if($this->session->userdata('error') == TRUE){ ?>
        $(document).ready(function () {
            Swal.fire({
              position: 'center',
              type: 'error',
              title: 'Gagal Disimpan !',
              showConfirmButton: false,
              timer: 1500
          });
        });
    <?php } ?>

</script>
</body>

</html>