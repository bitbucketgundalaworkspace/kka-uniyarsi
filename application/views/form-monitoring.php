<?php 
$this->load->view('header.php');
?>

<!-- COUNTERS -->
<section class="text-light background-overlay-dark" style="padding: 40px 0 !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3>Formulir Monitoring Universitas YARSI</h3>
                    <div class="seperator seperator-small"></div>
                    <h4>Form Monitoring</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: COUNTERS -->

<!-- Page Content -->
<section id="page-content" class="no-sidebar">
    <div class="container">

        <div class="row">
            <h2>Formulir Monitoring Kerjasama</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis nibh sit amet massa fringilla, vitae blandit velit tincidunt. Nunc auctor scelerisque urna a suscipit. Maecenas vulputate hendrerit dui, non interdum mi condimentum quis. Vivamus id arcu sit amet ligula viverra aliquam. Cras pulvinar risus nulla, et commodo magna sollicitudin vel. Vivamus arcu urna, dapibus ac iaculis facilisis, efficitur in purus. Nulla ac orci nec eros tempus aliquet in ut mauris. Suspendisse congue neque felis, at viverra neque fermentum id. Quisque ultricies scelerisque suscipit. Morbi dictum ex at neque suscipit, congue tincidunt lectus vestibulum. Curabitur congue ante et ipsum iaculis, nec lobortis libero congue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Interdum et malesuada fames ac ante ipsum primis in faucibus.
                <br><br>
            In hac habitasse platea dictumst. Praesent nec tellus vitae elit tristique rutrum at facilisis enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum sed maximus orci, in viverra enim. Donec ac dui et nulla dignissim commodo ac vitae erat. Quisque porttitor est ut nisi malesuada dapibus. Maecenas et odio diam. </p>
            <h5 style="font-weight: 500">Silahkan click link di bawah untuk melakukan download pada Formulir Monitoring</h5>
        </div>
        <div class="row">
            <a class="btn btn-rounded" href="<?php echo base_url('dokumen/Formulir-Monitoring-dan-Evaluasi-YARSI.docx');?>" target="_blank"><i class="fa fa-download"></i> Download File</a>
        </div>
        
        
    </div>
</section>
<!-- end: Page Content -->

<?php 
$this->load->view('footer.php');
?>   
