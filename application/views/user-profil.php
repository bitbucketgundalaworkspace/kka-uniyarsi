<?php 
$this->load->view('header.php');
?>

<!-- COUNTERS -->
<section class="text-light background-overlay-dark" style="padding: 40px 0 !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3>Upload Formulir Ajuan Universitas YARSI</h3>
                    <div class="seperator seperator-small"></div>
                    <h4>Upload Form Ajuan</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: COUNTERS -->

<!-- Page Content -->
<section id="page-content" class="no-sidebar">
    <div class="container">
        <!-- DataTable -->
        <div class="row mb-3">
            <div class="col-lg-6">
                <h4>Upload Formulir Ajuan</h4>
                <p>Dashboard Ajuan Kerjasama</p>
            </div>
            <div class="col-lg-6 text-right">
                <button data-target="#modal-upload" data-toggle="modal" href="#" type="button" class="btn btn-light showAdd" style="background-color: #fec51c"><i class="fa fa-plus"></i> Upload Ajuan</button>
                <div id="export_buttons" class="mt-2"></div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table id="datatable" class="table table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Dokumen Ajuan</th>
                            <th>Tanggal Ajuan</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; foreach ($ajuan as $a) : ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td>
                                <a href="<?php echo base_url('./uploads/ajuan/').$a['dokumen']; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/images/msword.png') ?>" title="Dokumen Ajuan Kerjasama" width="40"/>
                                </a>
                                <?php if (!empty($a['dok_kepuasan'])){ ?>
                                    <a href="<?php echo base_url('./uploads/kepuasan/').$a['dok_kepuasan']; ?>" target="_blank">
                                        <img src="<?php echo base_url('assets/images/msword2.png') ?>" title="Dokumen Kepuasan" width="40"/>
                                    </a>
                                <?php } ?>
                                <?php if ($a['jenis_ajuan'] == "Perbaikan Ajuan"){
                                    echo $a['jenis_ajuan'];
                                } ?>
                            </td>
                            <td><?php if ($a['status'] == 'Diterima') {
                                $color = '#b8f9bf';
                            }elseif ($a['status'] == 'Ditolak') {
                                $color = '#fcd4d4';
                            }elseif ($a['status'] == 'Perbaiki'){
                                $color = '#FFE700';
                            }else{
                                $color = '#00FFFF';
                            } $tgl = explode(" ", $a['tgl_ajuan']); echo date_indo($tgl[0]); ?></td>
                            <td>
                                <a <?php if($a['status'] != 'Tolak') { echo "data-target=#modal data-toggle=modal"; } ?> href="#" class="mod" data-id="<?php echo $a['id']; ?>" data-status="<?php echo $a['status']; ?>" data-catatan="<?php echo $a['catatan']; ?>"><span class="badge badge-pill" style="background-color: <?php echo $color; ?>"><?php $sta = $a['status']; echo $a['status']; ?></span></a>
                            </td>
                            <td>
                                <?php if ($a['status'] == 'Diterima') { ?>
                                  <a data-target="#modal-kepuasan" data-toggle="modal" href="#" class="moddd" data-id="<?php echo $a['id']; ?>"><span class="badge badge-pill" style="background-color: #71c6f7;" data-id="<?php echo $a['id']; ?>">Upload Form Kepuasan</span></a> 
                              <?php }    ?>
                          </td>
                      </tr>
                  <?php endforeach; ?>
              </tbody>
          </table>
      </div>
  </div>
  <!-- end: DataTable -->
</div>
</section>
<!-- end: Page Content -->

<!--Modal Review Ajuan-->
<div class="modal fade" id="modal" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true"
style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="modal-label">Review Ajuan</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <form method="POST" action="<?php echo base_url('Formulir/update_ajuan'); ?>" id="" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h5>Status</h5>
                        <span class="badge badge-pill" style="background-color: <?php echo $color; ?>" id="spn"></span>
                        <hr>

                        <div class="cat">
                            <h5>Balasan</h5>
                            <p id="catat"></p>
                            <hr>

                            <input type="hidden" name="id" class="id" value="">
                        </div>

                        <p id="menunggu">Saat ini ajuan anda sedang dalam proses, harap menunggu.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-b" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
</div>
<!-- end: Modal -->

<!--Modal Upload Ajuan-->
<div class="modal fade" id="modal-upload" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true"
style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="modal-label">Upload Ajuan </h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">

            <div class="row">
                <div class="col-md-12">
                   <form method="POST" action="<?php echo base_url('Formulir/upload_ajuan'); ?>" id="" enctype="multipart/form-data">
                    <div class="form-group">
                       <input type="hidden" name="ajuan" value="Ajuan Baru">
                       <label for="exampleInputEmail1">Jenis Ajuan</label>
                       <select class="form-control" required="" name="jenis">
                        <option value="">--Silahkan Pilih--</option>
                        <option value="Ajuan Baru">Ajuan Baru</option>
                        <option value="Perbaikan Ajuan">Perbaikan Ajuan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Upload Dokumen</label>
                    <input type="file" class="form-control" id="file_ajuan" name="file_ajuan" placeholder="Nomor Kerjasama" value="" accept=".docm">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-b" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-b">Simpan</button>
    </div>
</form>
</div>
</div>
</div>
<!-- end: Modal -->

<!--Modal Upload Ajuan-->
<div class="modal fade" id="modal-kepuasan" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true"
style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="modal-label">Upload Form Kepuasan</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">

            <div class="row">
                <div class="col-md-12">
                   <form method="POST" action="<?php echo base_url('Formulir/update_kepuasan'); ?>" id="" enctype="multipart/form-data">
                    <input class="idd" type="hidden" name="id" value="">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Upload Dokumen</label>
                        <input type="file" class="form-control" id="file_ajuan" name="file_ajuan" placeholder="Nomor Kerjasama" value="" accept=".*">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-b" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-b">Simpan</button>
        </div>
    </form>
</div>
</div>
</div>
<!-- end: Modal -->

<?php 
$this->load->view('footer.php');
?>


<!-- Export Datatable -->
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable( {
            buttons: [
            {
                extend: 'print',
                title: 'Berkas Ajuan',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }

            },{
                extend: 'pdf',
                title: 'Berkas Ajuan',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            },{
                extend: 'excel',
                title: 'Berkas Ajuan',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }, {
                extend: 'copy',
                title: 'Berkas Ajuan',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }
            ]
        } );

        table.buttons().container().appendTo( '#export_buttons' );
        $("#export_buttons .btn").removeClass('btn-secondary').addClass('btn-light');
    } );

    $(document).on("click", ".moddd", function () {
        $(".idd").val($(this).attr("data-id"));
    });
    $(document).on("click", ".mod", function () {
        $('#spn').text($(this).attr("data-status"));
        $('#catat').text($(this).attr("data-catatan"));
        if($(this).attr("data-status") != null){
            $('.cat').show();
            $('#menunggu').hide();
        }
        if($(this).attr("data-status") == 'Diproses'){
            $('#menunggu').show();
            $('.cat').hide();    
        }
        if($(this).attr("data-status") == 'Perbaiki'){
            $('.sta').show();
        }else{
            $('.sta').hide();    
        }
        $("#modal .modal-body .id").val($(this).attr("data-id"));
    });
</script>

