<?php
$this->load->view('header.php');
?>

<!-- COUNTERS -->
<section style="padding: 40px 0 !important; background-color: #fec51c !important">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="text-center">
          <h3>LOGIN</h3>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end: COUNTERS -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
        <form role="form" class="register-form" method="post" action="<?php echo base_url('auth/login'); ?>" accept-charset="utf-8">
          <h2>Masuk <small>ke Sistem</small></h2>
          <hr class="colorgraph">

          <div class="form-group">
            <input type="text" name="identity" id="identity" class="form-control input-lg" placeholder="Email">
          </div>
          <div class="form-group">
            <input type="password" class="form-control input-lg" id="password" name="password" placeholder="Password">
          </div>
          <div class="form-group">
            <input type="checkbox" value="1" name="remember" id="remember">   Remember me
          </div>
          <hr class="colorgraph">
          <div class="row">
            <div class="col-xs-12 col-md-6"><input type="submit" name="submit" value="Masuk" class="btn btn-primary btn-block btn-lg"></div>
            <!--<div class="col-xs-12 col-md-6">Don't have an account? <a href="register.html">Register</a></div>-->
          </div>
        </form>
      </div>
    </div>

  </div>
</section>

<?php
$this->load->view('footer.php');
?>

