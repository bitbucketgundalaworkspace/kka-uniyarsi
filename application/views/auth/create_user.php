<?php 
$this->load->view('header.php');
?>

<!-- COUNTERS -->
<section style="padding: 40px 0 !important; background-color: #fec51c !important">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="text-center">
          <h3>Tambah User</h3>
          <div class="seperator seperator-small"></div>
          <h4>Halaman Admin</h4>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end: COUNTERS -->

<section id="content">
  <div class="container">

    <h1><?php echo lang('create_user_heading');?></h1>
    <p><?php echo lang('create_user_subheading');?></p>

    <hr>

    <div id="infoMessage"><?php echo $message;?></div>

    <form action="<?php echo base_url('auth/create_user'); ?>" method="post">
      <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Depan</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="" name="first_name" placeholder="Nama Depan" required>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">Nama Belakang</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="" name="last_name" placeholder="Nama Belakang">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">Divisi</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="" name="company" placeholder="Divisi" required>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">Jabatan</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="" name="jabatan" placeholder="Jabatan">
        </div>
      </div>
      <!-- <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">Pihak internal</label>
        <div class="col-sm-10">
          <select name="internal" id="">
            <option disabled selected>--Silahkan Pilih---</option>
            <?php $query = $this->db->get('tb_pihakinternal')->result_array(); 
            foreach ($query as $r) { ?>
              <option value="<?php echo $r['nama_internal']; ?>"><?php echo $r['nama_internal']; ?></option>
            <?php } ?>
          </select>
        </div>
      </div> -->
      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" id="" name="email" placeholder="Email" required>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">Telepon</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="" name="phone" placeholder="Telepon">
        </div>
      </div>
      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="" name="password" placeholder="Password" required>
        </div>
      </div>
      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">Confirm Password</label>
        <div class="col-sm-10">
          <input type="password" class="form-control" id="" name="password_confirm" placeholder="Confirm Password" required>
        </div>
      </div>
      <div class="form-group row">
        <div class="col-sm-10">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </form>

  </div>
</section>

<?php 
$this->load->view('footer.php');
?>    

