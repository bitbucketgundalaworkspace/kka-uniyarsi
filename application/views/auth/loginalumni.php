<?php
$this->load->view('header.php');
?>

<section id="inner-headline">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url('home');?>"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
          <li><a href="#">Login Alumni</a><i class="icon-angle-right"></i></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section id="content">
  <div class="container">

    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
        <form role="form" class="register-form" method="post" action="<?php echo base_url('alumni/detail'); ?>" accept-charset="utf-8">
          <h2>Masuk <small>khusus Alumni</small></h2>
          <hr class="colorgraph">

          <div class="form-group">
            <input type="text" name="identity" id="identity" class="form-control input-lg" placeholder="Masukkan NPM">
          </div>
          <div class="form-group">
            <input type="password" class="form-control input-lg" id="password" name="password" placeholder="Masukkan Password">
          </div>
          <div class="form-group">
            <input type="checkbox" value="1" name="remember" id="remember">   Remember me
          </div>
          <hr class="colorgraph">
          <div class="row">
            <div class="col-xs-12 col-md-6"><input type="submit" name="submit" value="Masuk" class="btn btn-primary btn-block btn-lg"></div>
            <!--<div class="col-xs-12 col-md-6">Don't have an account? <a href="register.html">Register</a></div>-->
          </div>
        </form>
      </div>
    </div>

  </div>
</section>

<?php
$this->load->view('footer.php');
?>

