<div id="infoMessage"><?php echo $message;?></div>

<?php 
$this->load->view('header.php');
?>

<!-- COUNTERS -->
<section style="padding: 40px 0 !important; background-color: #fec51c !important">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3>Data User</h3>
                    <div class="seperator seperator-small"></div>
                    <h4>Halaman Admin</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: COUNTERS -->

<!-- Page Content -->
<section id="page-content" class="no-sidebar">
    <div class="container">
        <!-- DataTable -->
        <div class="row mb-3">
            <div class="col-lg-6">
                <h4>Data User</h4>
            </div>
            <div class="col-lg-6 text-right">
                <a class="btn btn-light" style="background-color: #fec51c" href="<?php echo base_url('auth/create_user');?>"><i class="fa fa-plus"></i> Tambah Data</a>
                <div id="export_buttons" class="mt-2"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table id="datatable" class="table table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>Nama Lengkap</th>
                            <th>Email</th>
                            <th>Nomor Tlp</th>
                            <th>Divisi</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th class="noExport">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $user):?>
                            <tr>
                                <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?> <?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
                                <td>
                                    <?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?>
                                </td>
                                <td><?php echo htmlspecialchars($user->phone,ENT_QUOTES,'UTF-8');?></td>
                                <td>
                                    <?php echo htmlspecialchars($user->company,ENT_QUOTES,'UTF-8');?>
                                    <br><hr>
                                    Jabatan : <?php echo htmlspecialchars($user->jabatan,ENT_QUOTES,'UTF-8');?>
                                    <br><hr>
                                    Operator dari : <?php echo htmlspecialchars($user->pihak_internal,ENT_QUOTES,'UTF-8');?>
                                </td>
                                <td>
                                    <?php foreach ($user->groups as $group):?>
                                        <?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
                                    <?php endforeach?>
                                </td>
                                <td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
                                <td><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?> </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DataTable -->

    </div>
</section>
<!-- end: Page Content -->

<?php 
$this->load->view('footer.php');
?>

<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable( {
            buttons: [
            {
                extend: 'print',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }

            },{
                extend: 'pdf',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            },{
                extend: 'excel',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }, {
                extend: 'copy',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }
            ]
        } );

        table.buttons().container().appendTo( '#export_buttons' );

        $("#export_buttons .btn").removeClass('btn-secondary').addClass('btn-light');
    } );
</script>
