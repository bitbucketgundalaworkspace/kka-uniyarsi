<?php 
$this->load->view('header.php');
?>

<!-- COUNTERS -->
<section style="padding: 40px 0 !important; background-color: #fec51c !important">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="text-center">
          <h3>Edit User</h3>
          <div class="seperator seperator-small"></div>
          <h4>Halaman Admin</h4>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end: COUNTERS -->

<section id="content">
  <div class="container">

    <h1><?php echo lang('edit_user_heading');?></h1>
    <p><?php echo lang('edit_user_subheading');?></p>

    <hr>

    <div id="infoMessage"><?php echo $message;?></div>

    <?php echo form_open(uri_string());?>
    <div class="form-group row">
      <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Depan</label>
      <div class="col-sm-10">
        <?php echo form_input($first_name);?>
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label">Nama Belakang</label>
      <div class="col-sm-10">
        <?php echo form_input($last_name);?>
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label">Divisi</label>
      <div class="col-sm-10">
        <?php echo form_input($company);?>
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label">Jabatan</label>
      <div class="col-sm-10">
        <?php echo form_input($jabatan);?>
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label">Pihak internal</label>
      <div class="col-sm-10">
        <select name="internal" id="" required>
          <option disabled selected value="">--Silahkan Pilih---</option>
          <option value="" style="background-color: #ddedf3">Bukan Akun Operator</option>
          <?php $query = $this->db->get('tb_pihakinternal')->result_array(); 
          foreach ($query as $r) { ?>
            <option value="<?php echo $r['nama_internal']; ?>"><?php echo $r['nama_internal']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label">Email</label>
      <div class="col-sm-10">
        <?php echo form_input($email);?>
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label">Telepon</label>
      <div class="col-sm-10">
        <?php echo form_input($phone);?>
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
      <div class="col-sm-10">
        <?php echo form_input($password);?>
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label">Confirm Password</label>
      <div class="col-sm-10">
        <?php echo form_input($password_confirm);?>
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword3" class="col-sm-2 col-form-label">Role User</label>
      <div class="col-sm-10">
        <?php if ($this->ion_auth->is_admin()): ?>
          <?php foreach ($groups as $group):?>
            <label class="checkbox">
              <?php
              $gID=$group['id'];
              $checked = null;
              $item = null;
              foreach($currentGroups as $grp) {
                if ($gID == $grp->id) {
                  $checked= ' checked="checked"';
                  break;
                }
              }
              ?>
              <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
              <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
            </label>
          <?php endforeach?>

        <?php endif ?>
      </div>
      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>
    </div>
    <div class="form-group row">
      <div class="col-sm-10">
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </div>
    <?php echo form_close();?>

  </div>
</section>

<?php 
$this->load->view('footer.php');
?>    

