<?php 
$this->load->view('header.php');
?>


<!-- COUNTERS -->
<section style="padding: 40px 0 !important; background-color: #fec51c !important">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3>Data User</h3>
                    <div class="seperator seperator-small"></div>
                    <h4>Halaman Admin</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: COUNTERS -->

<!-- Page Content -->
<section id="page-content" class="no-sidebar">
    <div class="container">
        <!-- DataTable -->
        <div class="row mb-3">
            <div class="col-lg-6">
                <h4>Data User</h4>
            </div>
            <div class="col-lg-6 text-right">
                <a class="btn btn-light" style="background-color: #fec51c" href="<?php echo base_url('auth/create_user');?>"><i class="fa fa-plus"></i> Tambah Data</a>
                <div id="export_buttons" class="mt-2"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table id="datatable" class="table table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Email</th>
                            <th>Nomor Tlp</th>
                            <th>Role</th>
                            <th class="noExport">Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- end: DataTable -->

    </div>
</section>
<!-- end: Page Content -->

<?php 
$this->load->view('footer.php');
?>

<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#example").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#example_filter input')
                .off('.DT')
                .on('keyup.DT', function(e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            scrollX: true,
            bAutoWidth: true,
            processing: true,
            serverSide: true,
            ajax: {"url": "user/data", "type": "POST"},
            columns: [
            {
                "data": "first_name",
                "orderable": false
            },
            {"data": "first_name"},
            {"data": "email"},
            {"data": "phone"},
            {"data": "company"},
            {"data": "Aksi"}
            ],
            order: [[1, 'asc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });

</script>  