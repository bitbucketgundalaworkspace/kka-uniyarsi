<?php 
$this->load->view('header.php');
?>

<!-- COUNTERS -->
<section class="text-light background-overlay-dark" style="padding: 40px 0 !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3>Formulir Ajuan Universitas YARSI</h3>
                    <div class="seperator seperator-small"></div>
                    <h4>Form Ajuan</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: COUNTERS -->

<!-- Page Content -->
<section id="page-content" class="no-sidebar">
    <div class="container">

        <div class="row">
            <h2>Formulir Ajuan Kerjasama</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis nibh sit amet massa fringilla, vitae blandit velit tincidunt. Nunc auctor scelerisque urna a suscipit. Maecenas vulputate hendrerit dui, non interdum mi condimentum quis. Vivamus id arcu sit amet ligula viverra aliquam. Cras pulvinar risus nulla, et commodo magna sollicitudin vel. Vivamus arcu urna, dapibus ac iaculis facilisis, efficitur in purus. Nulla ac orci nec eros tempus aliquet in ut mauris. Suspendisse congue neque felis, at viverra neque fermentum id. Quisque ultricies scelerisque suscipit. Morbi dictum ex at neque suscipit, congue tincidunt lectus vestibulum. Curabitur congue ante et ipsum iaculis, nec lobortis libero congue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Interdum et malesuada fames ac ante ipsum primis in faucibus.
                <br><br>
            In hac habitasse platea dictumst. Praesent nec tellus vitae elit tristique rutrum at facilisis enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum sed maximus orci, in viverra enim. Donec ac dui et nulla dignissim commodo ac vitae erat. Quisque porttitor est ut nisi malesuada dapibus. Maecenas et odio diam. </p>
            <h5 style="font-weight: 500">Silahkan click link di bawah untuk melakukan download pada Formulir Ajuan</h5>
        </div>
        <div class="row">
            <a class="btn btn-rounded" href="<?php echo base_url('dokumen/Formulir-Pengajuan-Kerjasama-YARSI.docm');?>" target="_blank"><i class="fa fa-download"></i> Download File</a>
        </div>
        <hr>
        <div class="row">
            <h2>Kirim Formulir Ajuan</h2>
            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis nibh sit amet massa fringilla, vitae blandit velit tincidunt. Nunc auctor scelerisque urna a suscipit. Maecenas vulputate hendrerit dui, non interdum mi condimentum quis. Vivamus id arcu sit amet ligula viverra aliquam. Cras pulvinar risus nulla, et commodo magna sollicitudin vel. Vivamus arcu urna, dapibus ac iaculis facilisis, efficitur in purus. Nulla ac orci nec eros tempus aliquet in ut mauris. Suspendisse congue neque felis, at viverra neque fermentum id. Quisque ultricies scelerisque suscipit. Morbi dictum ex at neque suscipit, congue tincidunt lectus vestibulum. Curabitur congue ante et ipsum iaculis, nec lobortis libero congue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Interdum et malesuada fames ac ante ipsum primis in faucibus.
            </p>
            <?php if(isset($_SESSION['group_user']) != 1 || isset($_SESSION['group_user']) != 2) { ?>
            <h5 style="font-weight: 500">Silahkan <a data-target="#modal" data-toggle="modal" href="#" style="color: #00aa13"> Login </a> terlebih dahulu atau <a data-target="#modal-reg" data-toggle="modal" href="#" style="color: #00aa13"> Daftar </a> jika belum memiliki Akun </h5>
            <?php } else { ?>
                <h5 style="font-weight: 500">Silahkan Isi Formulir Ajuan<a href="<?php echo base_url('Formulir'); ?>" style="color: #00aa13"> disini.
            <?php } ?>
        </div>
        <div class="row">

        </div>
        
        
    </div>
</section>
<!-- end: Page Content -->

<!--Modal Login-->
<div class="modal fade" id="modal" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true"
style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="modal-label">Masuk ke Sistem</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <form role="form" class="register-form" method="post" action="<?php echo base_url('auth/login'); ?>" accept-charset="utf-8">
                      <div class="form-group">
                        <input type="text" name="identity" id="identity" class="form-control input-lg" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="password" name="password" placeholder="Password">
                    </div>  
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-b" data-dismiss="modal">Close</button>
        <button type="Submit" class="btn btn-primary btn-b">Masuk</button>
    </div>
    </form>
</div>
</div>
</div>
<!-- end: Modal -->

<!--Modal Regist-->
<div class="modal fade" id="modal-reg" tabindex="-1" role="modal" aria-labelledby="modal-label" aria-hidden="true"
style="display: none;">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="modal-label">Registrasi Akun</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
        <form action="<?php echo base_url('auth/create_user'); ?>" method="post">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                 <form method="POST" action="#" id="" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Depan</label>
                        <input type="text" class="form-control" id="first_name" placeholder="Masukkan Nama Anda" value="" name="first_name" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Belakang</label>
                        <input type="text" class="form-control" id="first_name" placeholder="Masukkan Nama Anda" value="" name="last_name" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Alamat</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukkan Alamat" value="" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nomor Hp / Telepon</label>
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Masukkan No Tlp" value="" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Unit Kerja</label>
                        <input type="text" class="form-control" id="unit_kerja" name="company" placeholder="Masukkan Unit Kerja" value="" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Jabatan</label>
                        <input type="text" class="form-control" id="company" name="jabatan" placeholder="Masukkan Jabatan" value="" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email" value="" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Password" value="" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Confirm Password</label>
                        <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Masukkan Password" value="" required="required">
                    </div>
                    
                    <!-- <div class="form-group pull-right"><button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button></div> -->
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-b" data-dismiss="modal">Close</button>
        <button type="Submit" class="btn btn-primary btn-b">Simpan</button>
    </div>
</form>
</div>
</div>
</div>
<!-- end: Modal -->

<?php 
$this->load->view('footer.php');
?>


