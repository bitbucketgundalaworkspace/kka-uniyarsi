<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="adhikatdp" />
    <meta name="description" content="Sistem Informasi Kerjasama Universitas YARSI">
    <!-- Document title -->
    <title>Sistem Informasi Kerjasama - Universitas YARSI</title>
    <!-- Stylesheets & Fonts -->
    <link href="<?php echo base_url();?>assets/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png" />

    <!-- Template color -->
    <link href="<?php echo base_url();?>assets/css/color-variations/orange.css" rel="stylesheet" type="text/css" media="screen">

    <!-- DataTables css -->
    <link href='<?php echo base_url();?>assets/js/plugins/components/datatables/datatables.min.css' rel='stylesheet' />
</head>

<body>

    <!-- Body Inner -->
    <div class="body-inner">

        <!-- Header -->
        <header id="header" data-transparent="true" data-fullwidth="true">
            <div class="header-inner">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="<?php echo base_url('');?>" class="logo" data-src-dark="<?php echo base_url();?>assets/images/logo-dark.png">
                            <img src="<?php echo base_url();?>assets/images/sikerma-hi.png" alt="YARSI Logo">
                        </a>
                    </div>
                    <!--End: Logo-->

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->

                    <!--Navigation-->
                    <div id="mainMenu" class="light">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li class="dropdown"> <a href="<?php echo base_url('');?>">Beranda</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Pimpinan</a></li>
                                            <li><a href="#">Tugas & Fungsi</a></li>
                                            <li><a href="#">Program Kinerja & KAK</a></li>
                                            <li><a href="#">Indikator Kinerja Kegiatan</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"> <a href="#">Sikerma</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo base_url('form/ajuan');?>">Formulir Ajuan</a></li>
                                            <li><a href="<?php echo base_url('form/monitoring');?>">Formulir Monitoring</a></li>
                                            <li><a href="<?php echo base_url('form/kepuasan');?>">Formulir Kepuasan</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"> <a href="#">Kerjasama</a>
                                        <ul class="dropdown-menu">
                                            <li class="dropdown-submenu"><a href="#">Pemerintah</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="<?php echo base_url('dalamnegeri?idlm=1');?>">Dalam Negeri</a></li>
                                                    <li><a href="<?php echo base_url('luarnegeri?idlm=1');?>">Luar Negeri</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropdown-submenu"><a href="#">Institusi Pendidikan</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="<?php echo base_url('dalamnegeri?idlm=2');?>">Dalam Negeri</a></li>
                                                    <li><a href="<?php echo base_url('luarnegeri?idlm=2');?>">Luar Negeri</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropdown-submenu"><a href="#">Dunia Usaha</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="<?php echo base_url('dalamnegeri?idlm=3');?>">Dalam Negeri</a></li>
                                                    <li><a href="<?php echo base_url('luarnegeri?idlm=3');?>">Luar Negeri</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropdown-submenu"><a href="#">Organisasi</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="<?php echo base_url('dalamnegeri?idlm=4');?>">Dalam Negeri</a></li>
                                                    <li><a href="<?php echo base_url('luarnegeri?idlm=4');?>">Luar Negeri</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <?php if($this->session->userdata('group_user') == 2 || $this->session->userdata('group_user') == 3 || $this->session->userdata('group_user') == 1) { ?>
                                        <li><a href="<?php echo base_url('Formulir');?>">Ajuan</a></li>
                                    <?php } ?>
                                    <?php if(empty($this->session->userdata('group_user'))) { ?>
                                    <li class="dropdown"> <a href="#">Login</a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <div style="padding-left: 6px; padding-right: 6px; position: relative; line-height: 50px;">
                                                   <form role="form" class="register-form" method="post" action="<?php echo base_url('auth/login'); ?>" accept-charset="utf-8">
                                                    <h4>Login</h4>
                                                    <input type="text" name="identity" id="identity" placeholder="Email">
                                                    <input type="password" id="password" name="password" placeholder="Password">
                                                    <hr>
                                                    <div class="cart-buttons">
                                                        <input type="submit" name="submit" value="Masuk" class="btn btn-sm">
                                                    </div>
                                                </form>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <?php } else if($this->session->userdata('group_user') == 1 ) { ?>
                                    <li class="dropdown"> <a href="#">Admin</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo base_url('form/data_ajuan');?>">Data Ajuan</a></li>
                                            <li><a href="<?php echo base_url('auth');?>">Data User</a></li>
                                            <li><a href="<?php echo base_url('pihakinternal');?>">Data Pihak Internal</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="<?php echo base_url('auth/logout');?>">Logout</a></li>
                                <?php }else { ?>
                                    <li><a href="<?php echo base_url('auth/logout');?>">Logout</a></li>
                                <?php } ?>
                                         
                                   
                            </ul>
                        </nav>
                    </div>
                </div>
                <!--end: Navigation-->
            </div>
        </div>
    </header>
        <!-- end: Header -->