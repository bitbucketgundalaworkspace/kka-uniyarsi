<?php 
$this->load->view('header.php');
?>

<!-- COUNTERS -->
<section style="padding: 40px 0 !important; background-color: #fec51c !important">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3>Kerjasama Universitas YARSI - <?php echo $kategori; ?></h3>
                    <div class="seperator seperator-small"></div>
                    <h4>Kegiatan Dalam Negeri</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: COUNTERS -->

<!-- Page Content -->
<section id="page-content" class="no-sidebar">
    <div class="container">
        <!-- DataTable -->
        <div class="row mb-3">
            <div class="col-lg-6">
                <h4>Kegiatan Kerjasama <?php echo $kategori; ?></h4>
                <p>Data kegiatan kerjasama yang dilakukan oleh Universitas YARSI bersama <?php echo $kategori; ?></p>
            </div>
            <div class="col-lg-6 text-right">
                <!-- <button type="button" class="btn btn-light" style="background-color: #fec51c"><i class="fa fa-plus"></i> Tambah Data</button>
                    <div id="export_buttons" class="mt-2"></div> -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table id="datatable" class="table table-bordered table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th style="width: 2%">No</th>
                                <th style="width: 30%">Uraian Kegiatan</th>
                                <th style="width: 20%">Tanggal Kegiatan</th>
                                <th style="width: 20%">Presentase Kegiatan (%)</th>
                                <th style="width: 15%">Lampiran</th>
                                <th style="width: 13%" class="noExport">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; foreach ($datakeg as $d) : ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $d['uraian']; ?></td>
                                <td><?php echo date_indo($d['tanggal']); ?></td>
                                <td><?php echo $d['persentase']; ?> %</td>
                                <td>
                                    <?php if(!empty($d['file'])){?>
                                        <a href="<?php echo base_url('').$d['file']; ?>" target="_blank">
                                            <img src="<?php echo base_url('assets/images/availpdf.png') ?>" width="40" height="50"/>
                                        </a>
                                    <?php } else { ?> 
                                        <img src="<?php echo base_url('assets/images/unavailpdf.png') ?>" width="40" height="50"/> 
                                    <?php } ?>
                                </td>
                                <td> 
                                    <a class="ml-2 showEdit" data-id="<?php echo $d['id_kegiatandalam']; ?>" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                    <a class="ml-2 showDelete" data-id="<?php echo $d['id_kegiatandalam']; ?>" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end: DataTable -->

    </div>
</section>
<!-- end: Page Content -->

<?php 
$this->load->view('footer.php');
?> 

<!-- Modal Edit Kegiatan -->
<div class="modal fade" id="myModal2" role="dialog" >
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content" style="overflow: auto;padding-bottom: 15px;">
            <div class="modal-header">
                <h4 class="modal-title">Form Edit</h4>
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            </div>
            <div class="modal-body">

                <form method="POST" action="#" id="formAddEdit2" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Kegiatan</label>
                        <input type="date" class="form-control" name="tanggal" placeholder="" id="tanggal" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Uraian Kegiatan</label>
                        <textarea type="text" class="form-control" id="uraian" placeholder="" name="uraian" required="required"> </textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Persentase Kegiatan</label>
                        <div class="input-group">
                            <input type="number" class="form-control" placeholder="" id="persentase" name="persentase" aria-describedby="basic-addon2" min="0" max="100">
                            <span class="input-group-addon" id="basic-addon2">%</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Upload File</label>
                        <input type="file" class="form-control" id="file" name="file" placeholder="">
                    </div>
                    <input type="hidden" name="id_kegiatandalam" value="" id="id_kegiatandalam">
                    <div class="form-group pull-right"><button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button></div>
                </form>
            </div>
        </div>
    </div>
</div>      

<!-- Modal Delete -->
<div class="modal fade" id="myModalDelete" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="overflow: auto;padding-bottom: 15px;">
            <div class="modal-header">
                <h4 class="modal-title" id="titleHapus">Hapus Data?</h4>
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            </div>
            <div class="modal-body">
                <div class="pull-right"><a type="button" class="btn btn-danger" id="deleteButton" href=""<?php echo base_url('dalamnegeri');?>"">Ya</a>&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" id="cancelDeleteButton">Batal</button></div>
            </div>
        </div>

    </div>
</div>
<!--End Modal Delete--> 

<!-- CRUD -->
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var month = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', ' Agustus', 'September', 'Oktober', 'November', 'Desember'];

        function indonesia_date(date = '', time = true){
            if(time == true){
                d = date.split(' ');
                date = d[0];
            }

            d = date.split('-');

            date = d[2]+' '+month[parseInt(d[1])]+' '+d[0];

            return date;
        }
        function indonesia_date2(date = '', time = true){
            if(time == true){
                d = date.split(' ');
                date = d[0];
            }

            if(date != null)
                d = date.split('-');

            date = d[2]+' '+month[parseInt(d[1])]+' '+d[0];

            return date;
        }

        function monthDiff(d1, d2) {
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth() + 1;
            months += d2.getMonth();
            return months <= 0 ? 0 : months;
        }           

        var t = $("#example").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#example_filter input')
                .off('.DT')
                .on('keyup.DT', function(e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            scrollX: true,
            bAutoWidth: true,
            processing: true,
            serverSide: true,
            ajax: {"url": "<?php @$id = $this->uri->segment('3'); echo base_url('dalamnegeri/data_kegiatan/'.$id); ?>", "type": "POST"},
            columns: [
            {
                "data": "uraian",
                "orderable": false
            },
            {"data": "uraian"},
            {"data": "tanggal"},
            {"data": "persentase"},
            {"data": "file"},
            {"data": "Aksi"}
            ],
            order: [[1, 'asc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                var date = new Date();
                var year = date.getFullYear();
                var month = date.getMonth() + 1;
                if(month < 10){
                    month = "0" + month;
                }
                var day = date.getDate();
                var today = year +  '-' + month + '-' + day;
                    //console.log(today);
                    $('td:eq(0)', row).html(index);
                    $('td:eq(7)', row).css('background-color', 'Red');
                    $('td:eq(7)', row).css('color', 'White');
                    if(data.file == null){
                        $('td:eq(4)', row).html('<a>' + '<img src="<?php echo base_url('assets/img/pdf-empty.png') ?>" width="40" height="40"/>' + '</a>')
                    }else{
                        $('td:eq(4)', row).html('<a target="_blank" href="<?php echo base_url('');?>' + data.file + '">' + '<img src="https://c1.staticflickr.com/8/7014/6436110129_0ae969e102.jpg" width="40" height="40"/>' + '</a>')
                    }
                }
            });
    });

$(document).on("click", ".showDelete", function () {
    url = '<?php echo base_url("dalamnegeri/delete_kegiatan");?>'; 
    var id_kegiatandalam = $(this).data('id');
    $("#titleHapus").html("Anda yakin ingin menghapus ?");
    $('#myModalDelete').modal('show');

    $("#deleteButton").click(function(){
        $.post( url, { id_kegiatandalam : id_kegiatandalam }, function( data ) {
            if(data == "true"){
                // alert("berhasil di hapus");
                // $("#example").dataTable().fnDraw();
                $('#myModalDelete').modal('hide');
                $('#myModal2').modal('hide');
                Swal.fire({
                  position: 'center',
                  type: 'success',
                  title: 'Berhasil Disimpan !',
                  showConfirmButton: false,
                  timer: 1500
              });
                window.location.replace("<?php echo base_url('Dalamnegeri/lihatkegiatan/'.$this->uri->segment('3').'/'.$this->uri->segment('4')); ?>");
            }
            else{
                Swal.fire({
                  position: 'center',
                  type: 'success',
                  title: 'Gagal Disimpan !',
                  showConfirmButton: false,
                  timer: 1500
              });
                window.location.replace("<?php echo base_url('Dalamnegeri/lihatkegiatan/'.$this->uri->segment('3').'/'.$this->uri->segment('4')); ?>");    
            }
        });
    });
});

$(document).on("click", ".showEdit", function () {
    uri = '<?php echo base_url("dalamnegeri/update_kegiatan");?>'; 
    url = '<?php echo base_url("dalamnegeri/data_kegiatan_lihat");?>'; 
    var id_kegiatandalam = $(this).data('id');
    $.ajax({ url: url + '/' + id_kegiatandalam, 
        type: 'GET', 
        dataType: 'json', 
        success: function(result) { 
            var j = 1;
            console.log(result[0]);
            for (var i = 0; i < result.length; i++){
                $("#formAddEditTitle").html('Form Edit Kerjasama Dalam Negeri');
                $(".modal-body #uraian").val( result[i].uraian );
                $(".modal-body #tanggal").val( result[i].tanggal );
                $(".modal-body #persentase").val( result[i].persentase );
                $(".modal-body #id_kegiatandalam").val( id_kegiatandalam );
                $(".modal-body #formAddEdit2").attr('action', uri);
                $('#myModal2').modal('show');
            }
        }
    }); 
});

$("#formAddEdit2").submit(function(e){
    e.preventDefault();    
    var formData = new FormData(this);

    $.ajax({
        url: $(this).attr("action"),
        type: 'POST',
        data: formData,
        success: function (data) {
            if(data === "true"){
                // alert("data berhasil di simpan");
                // $("#example").dataTable().fnDraw();
                $('#myModal2').modal('hide');
                Swal.fire({
                  position: 'center',
                  type: 'success',
                  title: 'Berhasil Disimpan !',
                  showConfirmButton: false,
                  timer: 1500
              });
                window.location.replace("<?php echo base_url('Dalamnegeri/lihatkegiatan/'.$this->uri->segment('3').'/'.$this->uri->segment('4')); ?>");
            }
            else{
                Swal.fire({
                  position: 'center',
                  type: 'success',
                  title: 'Gagal Disimpan !',
                  showConfirmButton: false,
                  timer: 1500
              });
                window.location.replace("<?php echo base_url('Dalamnegeri/lihatkegiatan/'.$this->uri->segment('3').'/'.$this->uri->segment('4')); ?>");  
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});
</script>

<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable( {
            buttons: [
            {
                extend: 'print',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }

            },{
                extend: 'pdf',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            },{
                extend: 'excel',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }, {
                extend: 'copy',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }
            ]
        } );

        table.buttons().container().appendTo( '#export_buttons' );

        $("#export_buttons .btn").removeClass('btn-secondary').addClass('btn-light');
    } );
</script>

