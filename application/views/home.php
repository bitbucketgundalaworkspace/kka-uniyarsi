<?php 
$this->load->view('header.php');
?>

<!-- Inspiro Slider -->
<div id="slider" class="inspiro-slider slider-halfscreen arrows-large arrows-creative dots-creative" data-height-xs="360" data-autoplay-timeout="2600" data-animate-in="fadeIn" data-animate-out="fadeOut" data-items="1" data-loop="true" data-autoplay="true">
    <!-- Slide 1 -->
    <div class="slide background-image" style="background-image:url('<?php echo base_url();?>assets/homepages/corporate-v6/images/5.jpg');">
        <div class="container">
            <div class="slide-captions text-center">
                <!-- Captions -->
                <h2 class="text-lg m-b-0 text-dark">Pusat Kerja Sama</h2>
                <h2 class="text-medium text-dark">dan Hubungan Internasional</h2>
                <a class="btn btn-dark btn-outline" href="#">Kerjasama</a>
                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <!-- end: Slide 1 -->
    <!-- Slide 2 -->
    <div class="slide background-image" style="background-image:url('<?php echo base_url();?>assets/homepages/corporate-v6/images/6.jpg');">
        <div class="container">
            <div class="slide-captions text-left">
                <!-- Captions -->
                <h2 class="text-lg m-b-0 text-dark">Pusat Kerja Sama</h2>
                <h2 class="text-medium text-dark">dan Hubungan Internasional</h2>
                <a class="btn btn-dark btn-outline" href="#">Kerjasama</a>
                <!-- end: Captions -->
            </div>
        </div>
    </div>
    <!-- end: Slide 2 -->
</div>
<!--end: Inspiro Slider -->

<!-- About us -->
<section id="section-about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Bidang Kerjasama YARSI</h1>
                <div class="row">
                    <!-- features box -->
                    <div class="col-lg-6">
                        <p>We’re POLO, a creative agency located in the heart of New York city. A true story, that never been told!. Fusce id mi diam, non ornare orci. Pellentesque ipsum erat, facilisis ut venenatis eu.</p>
                        <p>Nulla varius consequat magna, id molestie ipsum volutpat quis. A true story, that never been told!. Fusce id mi diam, non ornare. Fusce id mi diam, non ornare orci. Pellentesque ipsum erat, facilisis ut venenatis eu.</p>
                        <a href="#services" class="btn btn-rounded">More info</a>
                    </div>
                    <!-- end: features box -->

                    <!-- Progress bar -->
                    <div class="col-lg-6">
                        <div class="p-progress-bar-container title-up small color">
                            <div class="p-progress-bar" data-percent="100" data-delay="0" data-type="%">
                                <div class="progress-title"><h6>Target Kerja Sama Luar Negeri 2019</h6></div>
                            </div>
                        </div>

                        <div class="p-progress-bar-container title-up small color">
                            <div class="p-progress-bar" data-percent="94" data-delay="100" data-type="%">
                                <div class="progress-title"><h6>Target Kerja Sama Dalam Negeri 2019</h6></div>
                            </div>
                        </div>

                        <div class="p-progress-bar-container title-up small color">
                            <div class="p-progress-bar" data-percent="89" data-delay="200" data-type="%">
                                <div class="progress-title"><h6>Target Produk Terhilirisasi 2019</h6></div>
                            </div>
                        </div>
                    </div>
                    <!-- end: Progress bar -->
                </div>
            </div>
            <!-- end features box -->
        </div>
    </div>
</section>
<!-- end: About us -->

<!-- SERVICES -->
<section class="background-grey">
    <div class="container">
        <div class="row">
            <div style="margin-bottom: 20px">
                <h1>Layanan Kami</h1>
                <p>Universitas YARSI dipenuhi oleh banyak pakar dan peneliti yang dapat mengembangkan bisnis dan memberikan saran dalam banyak bidang</p>
            </div>
            <!--Box 1-->
            <div class="row col-no-margin">    
                <div class="col-lg-3">
                    <div class="text-box hover-effect text-dark">
                        <h3>Kerjasama Internasional </h3>
                        <p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>
                    </a>
                </div>
            </div>
            <!--End: Box 1-->
            <!--Box 2-->
            <div class="col-lg-3">
                <div class="text-box hover-effect text-dark">
                    <h3>Kerjasama Nasional </h3>
                    <p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>
                </a>
            </div>
        </div>
        <!--End: Box 2-->
        <!--Box 3-->
        <div class="col-lg-3">
            <div class="text-box hover-effect text-dark">
                <h3>Hilirisasi <br>Produk</h3>
                <p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>
            </a>
        </div>
    </div>
    <!--End: Box 3-->
    <!--Box 4-->
    <div class="col-lg-3">
        <div class="text-box hover-effect text-dark">
            <h3>Korporasi Akademik</h3>
            <p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>
        </a>
    </div>
</div>
<!--End: Box 4-->
</div>
</div>
</section>
<!-- end: SERVICES -->

<!-- COUNTERS -->
<section class="text-light background-overlay-dark" data-parallax-image="images/parallax/5.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="text-center">
                    <?php $l = $this->db->get('ks_luar')->num_rows(); $d = $this->db->get('ks_dalam')->num_rows(); ?>
                    <div class="counter"> <span data-speed="3000" data-refresh-interval="50" data-to="<?php echo $l; ?>"
                        data-from="600" data-seperator="true"></span> </div>
                        <div class="seperator seperator-small"></div>
                        <h5>Kerjasama Luar Negeri</h5>
                    </div>
                </div>

                <div class="col-lg-6">

                    <div class="text-center">
                        <div class="counter"> <span data-speed="4500" data-refresh-interval="23" data-to="<?php echo $d; ?>"
                            data-from="100" data-seperator="true"></span> </div>
                            <div class="seperator seperator-small"></div>
                            <h5>Kerjasama Dalam Negeri</h5>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- end: COUNTERS -->

        <!-- chart -->
        <section id="page-content">
            <div class="container">
                <div class="row">
                    <div class="content col-lg-12">
                        <div class="chart-container">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end: chart -->

        <section>
            <div class="container">
             <!--Team members shadow-->
             <div class="text-center" style="margin-bottom: 20px">
                <h1>Tim Kami</h1> 
            </div>
            <div class="carousel team-members team-members-shadow" data-arrows="false" data-margin="20" data-items="3">

                <div class="team-member">
                    <div class="team-image">
                        <img src="<?php echo base_url();?>assets/images/team/6.jpg">
                    </div>
                    <div class="team-desc">
                        <h3>Alea Smith</h3>
                        <span>Software Developer</span>
                    </div>
                </div>

                <div class="team-member">
                    <div class="team-image">
                        <img src="<?php echo base_url();?>assets/images/team/6.jpg">
                    </div>
                    <div class="team-desc">
                        <h3>Ariol Doe</h3>
                        <span>Software Developer</span>
                    </div>
                </div>

                <div class="team-member">
                    <div class="team-image">
                        <img src="<?php echo base_url();?>assets/images/team/6.jpg">
                    </div>
                    <div class="team-desc">
                        <h3>Emma Ross</h3>
                        <span>Software Developer</span>
                    </div>
                </div>

            </div>
            <!--END: Team members shadow-->
        </div>
    </section>
    <!-- Modal -->
<?php if($this->session->userdata('group_user') == 3){ ?>
    <div id="modal-auto-open" class="modal modal-auto-open text-center cookie-notify"
    data-cookie-enabled="false" data-delay="2000">
    <h2 class="modal-title">Jangan Lupa</h2>
    <p>Tanggal 15 - 31 Desember jangan lupa mengupload formulir monitoring dan evaluasi kerjasama ya</p>
    <a class="btn btn-light modal-close" href="#">Tutup</a>
</div>
<?php } ?>
<?php if($this->session->userdata('group_user') == 1 && $cekajuan > 0){ ?>
    <div id="modal-auto-open" class="modal modal-auto-open text-center cookie-notify"
    data-cookie-enabled="false" data-delay="2000">
    <h2 class="modal-title">Jangan Lupa</h2>
    <p>Periksa Ajuan Yah !!!!</p>
    <a class="btn btn-light modal-close" href="#">Tutup</a>
</div>
<?php } ?>
<!--end: Modal -->

<?php 
$this->load->view('footer.php');
?>      

<!-- charts.js component-->
<script src="<?php echo base_url();?>assets/js/plugins/components/chartjs/chart.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/components/chartjs/utils.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/components/moment.min.js"></script>

<script>
    function createConfig(position) {
        return {
            type: 'line',
            data: {
                labels: <?php echo json_encode($month); ?>,
                datasets: [{
                    label: 'Luar Negeri',
                    borderColor: window.chartColors.red,
                    backgroundColor: window.chartColors.red,
                    data: <?php echo json_encode($luar); ?>,
                    fill: false,
                }, {
                    label: 'Dalam Negeri',
                    borderColor: window.chartColors.blue,
                    backgroundColor: window.chartColors.blue,
                    data: <?php echo json_encode($dalam); ?>,
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    /*text: 'Tooltip Position: ' + position*/
                },
                tooltips: {
                    position: position,
                    mode: 'index',
                    intersect: false,
                },
            }
        };
    }

    window.onload = function () {
        var container = document.querySelector('.chart-container');

        ['average'].forEach(function (position) {
            var div = document.createElement('div');
            div.classList.add('chart-container');

            var canvas = document.createElement('canvas');
            div.appendChild(canvas);
            container.appendChild(div);

            var ctx = canvas.getContext('2d');
            var config = createConfig(position);
            new Chart(ctx, config);
        });
    };
</script>


