<!-- Modal Tambah / Edit Kerjasama-->
<div class="modal fade" id="modal-kerjasama" role="dialog" >
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content" style="overflow: auto;padding-bottom: 15px;">
            <div class="modal-header">
                <h4 id="formAddEditTitle" class="modal-title">Tambah Kerjasama</h4>
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="#" id="form" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" id="pem" readonly value="Pemerintah" name="nama_eksternal" required="required">
                                <input type="file" class="form-control" id="dok_asli" name="dok_asli" style="display: none;">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" id="dal" readonly value="Dalam Negeri" name="nama_eksternal" required="required">
                                <input type="hidden" class="form-control" id="id_kat" readonly value="" name="id_kat" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Internal</label>
                        <select class="form-control" id="nama_internal" name="nama_internal" required="">
                            <option value="">--Silahkan Pilih--</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Instansi</label>
                        <input type="text" class="form-control" id="nama_eksternal" placeholder="Nama Instansi" value="" name="nama_eksternal" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Uraian Informasi</label>
                        <input type="text" class="form-control" id="deskripsi" name="deskripsi" placeholder="Masukkan Deskripsi" value="" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nomor</label>
                        <input type="text" class="form-control" id="nomor" name="nomor" placeholder="Nomor Kerjasama" value="">
                        <p style="font-weight: 500; font-size: 10px">*Boleh dikosongkan</p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Mulai</label>
                        <input type="date" class="form-control" id="start_date" name="start_date" placeholder="Tanggal Mulai" value="" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Berakhir</label>
                        <input type="date" class="form-control" id="end_date" name="end_date" placeholder="Tanggal Berakhir" value="" required="required">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Upload Dokumen</label>
                        <input type="file" class="form-control" id="dok_asli" name="dok_asli" placeholder="Nomor Kerjasama" value="" accept="application/pdf">
                    </div>
                    <input type="hidden" name="id_ksdalam" value="" id="id_ksdalam">
                    <input type="hidden" value="" name="id_aju" id="id_aju">
                    
                    <div class="form-group pull-right"><button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button></div>
                </form>
            </div>
        </div>
    </div>
</div>

