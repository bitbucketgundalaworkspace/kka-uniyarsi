<?php 
$this->load->view('header.php');
?>

<!-- COUNTERS -->
<section class="text-light background-overlay-dark" style="padding: 40px 0 !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h3>Kerjasama Universitas YARSI - <?php echo $tittle; ?></h3>
                    <div class="seperator seperator-small"></div>
                    <h4>Dalam Negeri</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end: COUNTERS -->

<!-- Page Content -->
<section id="page-content" class="no-sidebar">
    <div class="container">
        <!-- DataTable -->
        <div class="row mb-3">
            <div class="col-lg-6">
                <h4>Kerjasama <?php echo $tittle; ?></h4>
                <p>Data kerjasama yang dilakukan oleh Universitas YARSI bersama <?php echo $tittle; ?></p>
            </div>
            <div class="col-lg-6 text-right">
                <?php if(@$login != 0 && isset($_SESSION['user_id'])) { ?>
                    <button type="button" class="btn btn-light showAdd" style="background-color: #fec51c"><i class="fa fa-plus"></i> Tambah Data</button>
                <?php } ?>
                <div id="export_buttons" class="mt-2"></div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table id="datatable" class="table table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th style="width: 2%">No</th>
                            <th style="width: 10%">Internal</th>
                            <th style="width: 10%">Instansi</th>
                            <th style="width: 20%">Uraian Informasi</th>
                            <th style="width: 9%">Nomor Kerjasama</th>
                            <th style="width: 15%">Tanggal Kerjasama</th>
                            <th style="width: 7%">Dokumen</th>
                            <th style="width: 9%">Jml Kegiatan</th>
                            <?php if($this->session->userdata('group_user') == 1 || $this->session->userdata('group_user') == 3) { ?>
                                <th style="width: 8%" class="noExport">Aksi</th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $query = $this->db->get_where('users', array('id' => $this->session->userdata('user_id')))->result_array();
                        $i = 1; foreach ($datadalam as $d) : 
                        if(!empty($query[0]['pihak_internal'])){
                        if($d['nama_internal'] == $query[0]['pihak_internal'] && $this->session->userdata('group_user') == 3){
                            ?>
                            <tr>
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $d['nama_internal']++; ?></td>
                                <td><?php echo $d['nama_eksternal']++; ?></td>
                                <td><?php echo $d['deskripsi']++; ?></td>
                                <td><?php echo $d['nomor']++; ?></td>
                                <td>
                                    Mulai   : <br><?php echo $d['start_date']++; ?><br>
                                    Selesai : <br><?php echo $d['end_date']++; ?><br>
                                    <hr>
                                    Lama Kerjasama 
                                    <?php 
                                    $extgl1 = explode("-",$d['start_date']); 
                                    $extgl2 = explode("-",$d['end_date']);
                                    $th = ($extgl2[0] - $extgl1[0]) * 12;
                                    $bl = $extgl2[1] - $extgl1[1];
                                    $hr = $extgl2[2] - $extgl1[2];
                                    $tot = $th + $bl;
                                    echo $tot;
                                    ?> 
                                    Bulan
                                </td>
                                <td>
                                    <?php if(!empty($d['dok_asli'])){?>
                                        <a href="<?php echo base_url('').$d['dok_asli']; ?>" target="_blank">
                                            <img src="<?php echo base_url('assets/images/availpdf.png') ?>" title="MoU Kerjasama" width="40" height="50"/>
                                        </a>
                                    <?php } else { ?> 
                                        <img src="<?php echo base_url('assets/images/unavailpdf.png') ?>" title="MoU Kerjasama" width="40" height="50"/> 
                                    <?php } ?>
                                    <?php
                                    
                                    if($this->session->userdata('group_user') == 3 || $this->session->userdata('group_user') == 1){
                                        $query = $this->db->get_where('ajuan', array('id' => $d['id_ajuan']))->result_array();  
                                        if(!empty($query[0]['dok_kepuasan'])){ ?>
                                            <a href="<?php echo base_url('uploads/kepuasan/').$query[0]['dok_kepuasan']; ?>" target="_blank">
                                                <img src="<?php echo base_url('assets/images/msword2.png') ?>" title="Dokumen Kepuasan" width="40"/>
                                            </a>
                                        <?php } } ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo base_url('Dalamnegeri/lihatkegiatan/'.$this->input->get('idlm').'/').$d['id_ksdalam']; ?>"><?php echo $d['total_kegiatan']; ?></a> &nbsp;
                                        <?php 
                                        $dt = explode("-",date("Y-m-d"));
                                        $th = ($dt[0] - $extgl1[0]) * 12;
                                        $bl = $dt[1] - $extgl1[1];
                                        $tot1 = $th + $bl;
                                        if ($tot1 >= 6 &&  $d['total_kegiatan'] == 1) { ?>
                                            <span class="badge badge-pill" style="background-color: #b8f9bf">Hijau</span>    
                                        <?php } else if($tot1 <= 6 &&  $d['total_kegiatan'] == 1) { ?>
                                            <span class="badge badge-pill" style="background-color: #b8f9bf">Hijau</span> 
                                        <?php } else if($tot1 >= 6 && $tot1 <= 12 && $d['total_kegiatan'] == 0) { ?>
                                            <span class="badge badge-pill" style="background-color: #FFE700">Kuning</span>
                                        <?php } else if( $tot1 > 12){ ?>
                                            <span class="badge badge-pill" style="background-color: #fcd4d4">Merah</span>
                                        <?php } ?>
                                    </td>
                                    <?php if( $this->session->userdata('group_user') == 3) { ?>
                                        <td> 
                                            <a class="ml-2 myModal2 showAddKegiatan" data-toggle="tooltip" data-original-title="Tambah Kegiatan" data-id="<?php echo $d['id_ksdalam']; ?>"><i class="fa fa-plus"></i></a>
                                            <a href="<?php echo base_url('dalamnegeri/monev/'.$d['id_ksdalam']);?>" class="ml-2" data-toggle="tooltip" data-original-title="Lihat Monev" data-id="<?php echo $d['id_ksdalam']; ?>"><i class="fa fa-eye"></i></a>
                                            <?php 
                                            if($this->uri->segment('1') == 'dalamnegeri'){$ks = 1;}
                                            else{$ks = 2;}
                                            $query = $this->db->get_where('monev', array('id_ks' => $d['id_ksdalam'],'jenis_ks' => $ks))->result_array();
                                            if(!empty($query[0])){ echo '<span class="badge badge-pill" style="background-color: #00aa13; color: #FFF">Sudah Monev</span>';}
                                            ?>
                                        </td>
                                    <?php } else if($this->session->userdata('group_user') == 1 ){ ?>
                                        <td> 
                                            <a class="ml-2 myModal2 showAddKegiatan" data-toggle="tooltip" data-original-title="Tambah Kegiatan" data-id="<?php echo $d['id_ksdalam']; ?>"><i class="fa fa-plus"></i></a>
                                            <a href="<?php echo base_url('dalamnegeri/monev/'.$d['id_ksdalam']) ;?>" class="ml-2" data-toggle="tooltip" data-original-title="Lihat Monev" data-id="<?php echo $d['id_ksdalam']; ?>"><i class="fa fa-eye"></i></a>
                                            <a class="ml-2 showEdit" data-toggle="tooltip" data-original-title="Edit" data-id="<?php echo $d['id_ksdalam']; ?>"><i class="fa fa-edit"></i></a>
                                            <a class="ml-2 myModalDelete showDelete" data-id="<?php echo $d['id_ksdalam']; ?>" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                            <?php 
                                            if($this->uri->segment('1') == 'dalamnegeri'){$ks = 1;}
                                            else{$ks = 2;}
                                            $query = $this->db->get_where('monev', array('id_ks' => $d['id_ksdalam'],'jenis_ks' => $ks))->result_array();
                                            if(!empty($query[0])){ echo '<span class="badge badge-pill" style="background-color: #00aa13; color: #FFF">Sudah Monev</span>';}
                                            ?>
                                        </td>
                                    <?php }  ?>
                                </tr>

                                <!-- -------------- Untuk Non Operator -------------- -->
                            <?php }
                        } else if($this->session->userdata('group_user') != 3 ) { ?>
                                <tr>
                                    <td><?php echo $i++; ?></td>
                                    <td><?php echo $d['nama_internal']++; ?></td>
                                    <td><?php echo $d['nama_eksternal']++; ?></td>
                                    <td><?php echo $d['deskripsi']++; ?></td>
                                    <td><?php echo $d['nomor']++; ?></td>
                                    <td>
                                        Mulai   : <br><?php echo $d['start_date']++; ?><br>
                                        Selesai : <br><?php echo $d['end_date']++; ?><br>
                                        <hr>
                                        Lama Kerjasama 
                                        <?php 
                                        $extgl1 = explode("-",$d['start_date']); 
                                        $extgl2 = explode("-",$d['end_date']);
                                        $th = ($extgl2[0] - $extgl1[0]) * 12;
                                        $bl = $extgl2[1] - $extgl1[1];
                                        $hr = $extgl2[2] - $extgl1[2];
                                        $tot = $th + $bl;
                                        echo $tot;
                                        ?> 
                                        Bulan
                                    </td>
                                    <td>
                                        <?php if(!empty($d['dok_asli'])){?>
                                            <a href="<?php echo base_url('').$d['dok_asli']; ?>" target="_blank">
                                                <img src="<?php echo base_url('assets/images/availpdf.png') ?>" title="MoU Kerjasama" width="40" height="50"/>
                                            </a>
                                        <?php } else { ?> 
                                            <img src="<?php echo base_url('assets/images/unavailpdf.png') ?>" title="MoU Kerjasama" width="40" height="50"/> 
                                        <?php } ?>
                                        <?php

                                        if($this->session->userdata('group_user') == 3 || $this->session->userdata('group_user') == 1){
                                            $query = $this->db->get_where('ajuan', array('id' => $d['id_ajuan']))->result_array();  
                                            if(!empty($query[0]['dok_kepuasan'])){ ?>
                                                <a href="<?php echo base_url('uploads/kepuasan/').$query[0]['dok_kepuasan']; ?>" target="_blank">
                                                    <img src="<?php echo base_url('assets/images/msword2.png') ?>" title="Dokumen Kepuasan" width="40"/>
                                                </a>
                                            <?php } } ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('Dalamnegeri/lihatkegiatan/'.$this->input->get('idlm').'/').$d['id_ksdalam']; ?>"><?php echo $d['total_kegiatan']; ?></a> &nbsp;
                                            <?php 
                                            $dt = explode("-",date("Y-m-d"));
                                            $th = ($dt[0] - $extgl1[0]) * 12;
                                            $bl = $dt[1] - $extgl1[1];
                                            $tot1 = $th + $bl;
                                            if($this->session->userdata('group_user') == 1 || $this->session->userdata('group_user') == 2 || $this->session->userdata('group_user') == 3 ) { 
                                            if ($tot1 >= 6 &&  $d['total_kegiatan'] == 1) { ?>
                                                <span class="badge badge-pill" style="background-color: #b8f9bf">Hijau</span>    
                                            <?php } else if($tot1 <= 6 &&  $d['total_kegiatan'] == 1) { ?>
                                                <span class="badge badge-pill" style="background-color: #b8f9bf">Hijau</span> 
                                            <?php } else if($tot1 >= 6 && $tot1 <= 12 && $d['total_kegiatan'] == 0) { ?>
                                                <span class="badge badge-pill" style="background-color: #FFE700">Kuning</span>
                                            <?php } else if( $tot1 > 12){ ?>
                                                <span class="badge badge-pill" style="background-color: #fcd4d4">Merah</span>
                                            <?php } } ?>
                                        </td>
                                        <?php if( $this->session->userdata('group_user') == 3) { ?>
                                            <td> 

                                                <a class="ml-2 myModal2 showAddKegiatan" data-toggle="tooltip" data-original-title="Tambah Kegiatan" data-id="<?php echo $d['id_ksdalam']; ?>"><i class="fa fa-plus"></i></a>
                                                <a href="<?php echo base_url('dalamnegeri/monev/'.$d['id_ksdalam']);?>" class="ml-2" data-toggle="tooltip" data-original-title="Lihat Monev" data-id="<?php echo $d['id_ksdalam']; ?>"><i class="fa fa-eye"></i></a>
                                                <?php 
                                                if($this->uri->segment('1') == 'dalamnegeri'){$ks = 1;}
                                                else{$ks = 2;}
                                                $query = $this->db->get_where('monev', array('id_ks' => $d['id_ksdalam'],'jenis_ks' => $ks))->result_array();
                                                if(!empty($query[0])){ echo '<span class="badge badge-pill" style="background-color: #00aa13; color: #FFF">Sudah Monev</span>';}
                                                ?>
                                            </td>
                                        <?php } else if($this->session->userdata('group_user') == 1 ){ ?>
                                            <td> 
                                                <a class="ml-2 myModal2 showAddKegiatan" data-toggle="tooltip" data-original-title="Tambah Kegiatan" data-id="<?php echo $d['id_ksdalam']; ?>"><i class="fa fa-plus"></i></a>
                                                <a href="<?php echo base_url('dalamnegeri/monev/'.$d['id_ksdalam']) ;?>" class="ml-2" data-toggle="tooltip" data-original-title="Lihat Monev" data-id="<?php echo $d['id_ksdalam']; ?>"><i class="fa fa-eye"></i></a>
                                                <a class="ml-2 showEdit" data-toggle="tooltip" data-original-title="Edit" data-id="<?php echo $d['id_ksdalam']; ?>"><i class="fa fa-edit"></i></a>
                                                <a class="ml-2 myModalDelete showDelete" data-id="<?php echo $d['id_ksdalam']; ?>" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                                <?php 
                                                if($this->uri->segment('1') == 'dalamnegeri'){$ks = 1;}
                                                else{$ks = 2;}
                                                $query = $this->db->get_where('monev', array('id_ks' => $d['id_ksdalam'],'jenis_ks' => $ks))->result_array();
                                                if(!empty($query[0])){ echo '<span class="badge badge-pill" style="background-color: #00aa13; color: #FFF">Sudah Monev</span>';}
                                                ?>
                                            </td>
                                        <?php }  ?>
                                    </tr>
                                <?php } endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- end: DataTable -->
                <?php if($this->session->userdata('group_user') == 1 || $this->session->userdata('group_user') == 2 || $this->session->userdata('group_user') == 3 ) { ?>
                <div class="row">
                    <h4>Keterangan</h4>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <span class="badge badge-pill" style="background-color: #b8f9bf">Hijau</span> 
                        <p>6 bulan sudah ada kegiatan</p>
                    </div>
                    <div class="col-md-4">
                        <span class="badge badge-pill" style="background-color: #FFE700">Kuning</span> 
                        <p>6 - 12 bulan belum ada kegiatan</p>
                    </div>
                    <div class="col-md-4">
                        <span class="badge badge-pill" style="background-color: #fcd4d4">Merah</span>
                        <p>Lebih dari 12 bulan belum ada kegiatan</p>
                    </div>
                </div>
            <?php } ?>
            </div>
        </section>
        <!-- end: Page Content -->

        <?php 
        $this->load->view('footer.php');
        ?>   

        <!-- Modal Tambah / Edit Kerjasama-->
        <div class="modal fade" id="myModal" role="dialog" >
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content" style="overflow: auto;padding-bottom: 15px;">
                    <div class="modal-header">
                        <h4 id="formAddEditTitle" class="modal-title">Large Modal</h4>
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="#" id="formAddEdit" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Internal</label>
                                <select class="form-control" id="nama_internal" name="nama_internal" required="">
                                    <option value="">--Silahkan Pilih--</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Instansi</label>
                                <input type="text" class="form-control" id="nama_eksternal" placeholder="Nama Instansi" value="" name="nama_eksternal" required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Uraian Informasi</label>
                                <input type="text" class="form-control" id="deskripsi" name="deskripsi" placeholder="Masukkan Deskripsi" value="" required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nomor</label>
                                <input type="text" class="form-control" id="nomor" name="nomor" placeholder="Nomor Kerjasama" value="" required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tanggal Mulai</label>
                                <input type="date" class="form-control" id="start_date" name="start_date" placeholder="Tanggal Mulai" value="" required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tanggal Berakhir</label>
                                <input type="date" class="form-control" id="end_date" name="end_date" placeholder="Tanggal Berakhir" value="" required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Upload Dokumen</label>
                                <input type="file" class="form-control" id="dok_asli" name="dok_asli" placeholder="Nomor Kerjasama" value="" accept="application/pdf">
                            </div>
                            <input type="hidden" name="id_ksdalam" value="" id="id_ksdalam">
                            <input type="hidden" name="id_kat" value="<?php echo $this->input->get('idlm'); ?>" id="id_kat">
                            <div class="form-group pull-right"><button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Tambah Kegiatan Kerjasama -->
        <div class="modal fade" id="myModal2" role="dialog" >
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content" style="overflow: auto;padding-bottom: 15px;">
                    <div class="modal-header">
                        <h4 class="modal-title" id="formAddKegiatan">Form Tambah Kegiatan</h4>
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="#" id="formAddEdit2" enctype="multipart/form-data">
                            <input type="hidden" name="id_ksdalam" value="" id="id_ksdalam">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tanggal Kegiatan</label>
                                <input type="date" class="form-control" name="tanggal" placeholder="tanggal" value="" id="tanggal" required="required">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Uraian Kegiatan</label>
                                <textarea type="text" class="form-control" id="uraian" placeholder="Uraian Kegiatan" value="" name="uraian" required="required"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Persentase Kegiatan</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" name="persentase" id="persentase" placeholder="Persentase Kegiatan" aria-describedby="basic-addon2" min="0" max="100">
                                    <span class="input-group-addon" id="basic-addon2">%</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Upload File (Word/PDF)</label>
                                <input type="file" class="form-control" id="file" name="file" placeholder="Nomor Kerjasama" value="" required="required">
                            </div>
                            <input type="hidden" name="id_ksdalam" value="" id="id_ksdalam">
                            <input type="hidden" name="idlm" value="" id="idlm">
                            <div class="form-group pull-right"><button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Hapus Data -->
        <div class="modal fade" id="myModalDelete" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content" style="overflow: auto;padding-bottom: 15px;">
                    <div class="modal-header">
                        <h4 class="modal-title" id="titleHapus">Hapus Data?</h4>
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="pull-right"><a type="button" class="btn btn-danger" id="deleteButton" href="#">Ya</a>&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" id="cancelDeleteButton">Batal</button></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- CRUD -->
        <script type="text/javascript">
            $(document).ready(function() {

                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var month = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', ' Agustus', 'September', 'Oktober', 'November', 'Desember'];

                function indonesia_date(date = '', time = true){
                    if(time == true){
                        d = date.split(' ');
                        date = d[0];
                    }

                    if(date != null)
                        d = date.split('-');
                    date = d[2]+' '+month[parseInt(d[1])]+' '+d[0];
                    return date;
                }

                function indonesia_date2(date = '', time = true){
                    if(time == true){
                        d = date.split(' ');
                        date = d[0];
                    }

                    if(date != null)
                        d = date.split('-');
                    date = d[2]+' '+month[parseInt(d[1])]+' '+d[0];
                    return date;
                }

                function monthDiff(d1, d2) {
                    var months;
                    months = (d2.getFullYear() - d1.getFullYear()) * 12;
                    months -= d1.getMonth() + 1;
                    months += d2.getMonth();
                    return months <= 0 ? 0 : months;

                }

                function hasilnilai(nilai){
                    if (nilai == 1){
                        hasil = "Ada";
                    } else {
                        hasil = "Tidak"
                    }
                    return hasil;
                }

                var t = $("#example").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#example_filter input')
                        .off('.DT')
                        .on('keyup.DT', function(e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
                    },
                    oLanguage: {
                        sProcessing: "loading..."
                    },
                    scrollX: true,
                    bAutoWidth: true,
                    processing: true,
                    serverSide: true,

                    ajax: {"url": "<?php echo base_url('dalamnegeri/data/'.$id); ?>", "type": "POST"},
                    columns: [
                    {
                        "data": "nama_internal",
                        "orderable": false
                    },
                    {"data": "nama_internal"},
                    {"data": "nama_eksternal"},
                    {"data": "deskripsi"},
                    {"data": "nomor"},
                    {"data": "lama"},
                    {"data": "start_date"},
                    {"data": "end_date"},
                    {"data": "Dokumen"},
                    {"data": "total_kegiatan"}
                    <?php if($this->session->group_user == 1){ ?>,
                        {"data": "Aksi"}
                    <?php } ?>
                    ],
                    order: [[1, 'asc']],

                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        var date = new Date();
                        var year = date.getFullYear();
                        var month = date.getMonth() + 1;
                        if(month < 10){
                            month = "0" + month;
                        }
                        var day = date.getDate();
                        var today = year +  '-' + month + '-' + day;

                        $('td:eq(0)', row).html(index);

                        $('td:eq(5)', row).html((monthDiff(new Date(data.start_date), new Date(data.end_date)) + 1) + ' Bulan');

                        $('td:eq(6)', row).html(indonesia_date(data.start_date, false));

                        $('td:eq(7)', row).html(indonesia_date2(data.end_date, false));

                        /*$('td:eq(8)', row).html(hasilnilai(data.dok_asli));*/
                        if(data.dok_asli === null || data.dok_asli === ""){
                            $('td:eq(8)', row).html('<a>' + '<img src="<?php echo base_url('assets/img/pdf-empty.png') ?>" width="40" height="40"/>' + '</a>')
                        }else{
                            $('td:eq(8)', row).html('<a target="_blank" href="<?php echo base_url('');?>' + data.dok_asli + '">' + '<img src="<?php echo base_url('assets/img/icon-pdf.png') ?>" width="40" height="40"/>' + '</a>')
                        }
                        /*$('td:eq(8)', row).html('<a target="_blank" href="<?php echo base_url('pengembangan');?>">' + '<img src="https://c1.staticflickr.com/8/7014/6436110129_0ae969e102.jpg" width="40" height="40"/>' + '</a>');*/

                        $('td:eq(9)', row).html("<a href='<?php echo base_url("dalamnegeri/lihatkegiatan/");?>" + data.id_ksdalam + "'>" + data.total_kegiatan + "</a>");
            //console.log(today);

            var split = data.end_date.split("-");
            var split1 = today.split("-");
            var thn = split[0] - split1[0];
            var bln =(split[1] * 30) - (split1[1] * 30);
            var tgl = split[2] - split1[2];
            var jml = tgl + bln;
            if(jml >= 0 && jml <= 31 && thn == 0){
                $('td:eq(7)', row).css('background-color', 'Yellow');
            }
            if(data.end_date <= today){
                console.log(data.end_date <= today);
                $('td:eq(7)', row).css('background-color', 'Red');
                $('td:eq(7)', row).css('color', 'White');   
            }
            if(data.dok_asli = "0"){
                data.dok_asli = "Tidak";
            }
        }
    });
});

$(document).on("click", ".showAdd", function () {
    uri = '<?php echo base_url("dalamnegeri/insert");?>';
    $(".modal-body #nama_internal").html(''); 
    var option = '<option value="" disabled>--Silahkan Pilih--</option>';
    $.ajax({url: '<?php echo base_url("Pihakinternal/listData"); ?>',
        type: 'GET',
        dataType: 'json',
        success: function(result){
            result.forEach(function (item){
                option += '<option value="' + item.nama_internal + '">' + item.nama_internal + '</option>';
            });
            $(".modal-body #nama_internal").append(option);
        }
    });

    // $.ajax({url: '<?php //echo base_url("Pihakinternal/listData"); ?>',
    //     type: 'GET',
    //     dataType: 'json',
    //     success: function(result){
    //         console.log(result);
    //     }
    // });

    $("#formAddEditTitle").html('Form Tambah Kerjasama Dalam Negeri');
    $(".modal-body #nama_internal").val( '' );
    $(".modal-body #nama_eksternal").val( '' );
    $(".modal-body #deskripsi").val( '' );
    $(".modal-body #nomor").val( '' );
    $(".modal-body #lama").val( '' );
    $(".modal-body #start_date").val( '' );
    $(".modal-body #end_date").val( '' );
    $(".modal-body #dok_asli").val( '' );
    $("#formAddEdit").attr('action', uri);
    $('#myModal').modal('show');
});

$("#formAddEdit").submit(function(e){
    e.preventDefault();    
    var formData = new FormData(this);

    $.ajax({
        url: $(this).attr("action"),
        type: 'POST',
        data: formData,
        success: function (data) {
            if(data === "true"){
                // alert("data berhasil di simpan");
                // $("#example").dataTable().fnDraw();
                Swal.fire({
                  position: 'center',
                  type: 'success',
                  title: 'Berhasil Disimpan',
                  showConfirmButton: false,
                  timer: 1500
              });
                $('#myModal').modal('hide');
                window.location.replace("<?php echo base_url('Dalamnegeri?idlm='.$this->input->get('idlm')); ?>");
            }
            else{
                Swal.fire({
                  position: 'center',
                  type: 'error',
                  title: 'Gagal Disimpan !',
                  showConfirmButton: false,
                  timer: 1500
              });
                window.location.replace("<?php echo base_url('Dalamnegeri?idlm='.$this->input->get('idlm')); ?>");
                // alert("data gagal di simpan");  
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

$(document).on("click", ".showEdit", function () {
    uri = '<?php echo base_url("dalamnegeri/update");?>'; 
    url = '<?php echo base_url("dalamnegeri/data");?>'; 

    var option = '<option value="" disable>--Silahkan Pilih--</option>';
    $.ajax({url: '<?php echo base_url("Pihakinternal/listData");?>',
        type: 'GET',
        dataType: 'json',
        success: function(result){
            result.forEach(function (item){
                option += '<option value="' + item.nama_internal + '">' + item.nama_internal + '</option>';
            });
            // $(".modal-body #nama_internal").append(option);
            $(".modal-body #nama_internal").html(option);
        }
    }); 

    var id = $(this).data('id');
    $.ajax({ url: url + '/' + id, 
        type: 'GET', 
        dataType: 'json', 
        success: function(result) { 
            var j = 1;
            console.log(result[0]);
            for (var i = 0; i < result.length; i++){

                $("#formAddEditTitle").html('Form Edit Kerjasama Dalam Negeri');
                $(".modal-body #nama_internal").val( result[i].nama_internal );
                $(".modal-body #nama_eksternal").val( result[i].nama_eksternal );
                $(".modal-body #deskripsi").val( result[i].deskripsi );
                $(".modal-body #nomor").val( result[i].nomor );
                $(".modal-body #start_date").val( result[i].start_date );
                $(".modal-body #end_date").val( result[i].end_date );
                /*$(".modal-body #dok_asli").val( result[i].dok_asli );*/
                $(".modal-body #id_ksdalam").val( id );
                $("#formAddEdit").attr('action', uri);
                $('#myModal').modal('show');
            }
        }
    }); 
});

$(document).on("click", ".showDelete", function () {
    url = '<?php echo base_url("dalamnegeri/delete");?>';
    var id_ksdalam = $(this).data('id');
    $("#titleHapus").html("Anda yakin ?");
    $('#myModalDelete').modal('show');
    $("#deleteButton").click(function(){
        $.post( url, { id_ksdalam : id_ksdalam }, function( data ) {
            if(data == "true"){
                Swal.fire({
                  position: 'center',
                  type: 'success',
                  title: 'Berhasil Disimpan !',
                  showConfirmButton: false,
                  timer: 1500
              });
                // $("#example").dataTable().fnDraw();
                $('#myModalDelete').modal('hide');
                window.location.replace("<?php echo base_url('Dalamnegeri?idlm='.$this->input->get('idlm')); ?>");
            }
            else{
                Swal.fire({
                  position: 'center',
                  type: 'error',
                  title: 'Gagal Disimpan !',
                  showConfirmButton: false,
                  timer: 1500
              });
                window.location.replace("<?php echo base_url('Dalamnegeri?idlm='.$this->input->get('idlm')); ?>");
            }
        });
    });
});
</script>
<!-- end: CRUD -->

<script type="text/javascript">
    $('#myModal2 #file').on( 'change', function() {
     myfile= $( this ).val();
     var ext = myfile.split('.').pop();

     if(ext == 'png' || ext == 'JPEG' || ext == 'JPG' || ext == 'gif'){
        alert("Format Tidak Diijinkan !!!");
        $( this ).val("");
    }
});
</script>

<!-- Export Datatable -->
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable( {
            buttons: [
            {
                extend: 'print',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }

            },{
                extend: 'pdf',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            },{
                extend: 'excel',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }, {
                extend: 'copy',
                title: 'Kerjasama Dalam Negeri',
                exportOptions: {
                    columns: "thead th:not(.noExport)"
                }
            }
            ]
        } );

        table.buttons().container().appendTo( '#export_buttons' );
        $("#export_buttons .btn").removeClass('btn-secondary').addClass('btn-light');
    } );

    $(document).on("click", ".showAddKegiatan", function () {
        var id_ksdalam = $(this).data('id');
        uri = '<?php echo base_url("dalamnegeri/tambah_kegiatan");?>'; 
        $("#formAddKegiatan").html('Form Tambah Kegiatan Kerjasama Dalam Negeri');
        $(".modal-body #tanggal").val( '' );
        $(".modal-body #uraian").val( '' );
        $(".modal-body #persentase").val( '' );
        $(".modal-body #file").val( '' );
        $(".modal-body #id_ksdalam").val(id_ksdalam);
        $(".modal-body #idlm").val(<?php echo $this->input->get('idlm'); ?>);
        $("#formAddEdit2").attr('action', uri);
        console.log($(this).data('start'));
        $('#myModal2').modal('show');
    });
</script>

